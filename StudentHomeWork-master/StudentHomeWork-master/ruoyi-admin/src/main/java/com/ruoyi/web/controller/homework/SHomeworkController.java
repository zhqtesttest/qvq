package com.ruoyi.web.controller.homework;

import java.util.Date;
import java.util.List;

import com.ruoyi.common.core.domain.entity.SysDept;
import com.ruoyi.common.core.domain.entity.SysRole;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.ShiroUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.homework.domain.SHomework;
import com.ruoyi.homework.service.ISHomeworkService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 作业Controller
 * 
 * @author BahetCoder
 * @date 2021-06-04
 */
@Controller
@RequestMapping("/homework/leavehomework")
public class SHomeworkController extends BaseController
{
    private String prefix = "homework/leavehomework";

    @Autowired
    private ISHomeworkService sHomeworkService;

    @RequiresPermissions("homework:leavehomework:view")
    @GetMapping()
    public String leavehomework()
    {
        return prefix + "/leavehomework";
    }

    /**
     * 查询作业列表
     */
    @RequiresPermissions("homework:leavehomework:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SHomework sHomework)
    {
        Long userId = ShiroUtils.getUserId();//当前登录用户ID
        Long deptid = ShiroUtils.getSysUser().getDeptId();//当前用户部门ID
        SysUser sysUser = ShiroUtils.getSysUser();//获取用户对象
        List<SysRole> roles = sysUser.getRoles();
        SysRole sysRole = roles.get(0);
        //教师角色ID=3
        if(sysRole.getRoleId()==3){
            //如果是老师角色只显示当前老师操作的数据
            sHomework.setTeacherId(String.valueOf(userId));
        }//学生角色ID=4
        else if(sysRole.getRoleId()==4){
            //如果是学生角色只显示当前学生操作的数据
            sHomework.setStuclassid(deptid);
        }


        startPage();
        List<SHomework> list = sHomeworkService.selectSHomeworkList(sHomework);
        return getDataTable(list);
    }

    /**
     * 导出作业列表
     */
    @RequiresPermissions("homework:leavehomework:export")
    @Log(title = "作业", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SHomework sHomework)
    {
        List<SHomework> list = sHomeworkService.selectSHomeworkList(sHomework);
        ExcelUtil<SHomework> util = new ExcelUtil<SHomework>(SHomework.class);
        return util.exportExcel(list, "作业数据");
    }

    /**
     * 新增作业
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存作业
     */
    @RequiresPermissions("homework:leavehomework:add")
    @Log(title = "作业", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SHomework sHomework)
    {
        return toAjax(sHomeworkService.insertSHomework(sHomework));
    }

    /**
     * 修改作业
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        SHomework sHomework = sHomeworkService.selectSHomeworkById(id);
        mmap.put("sHomework", sHomework);
        return prefix + "/edit";
    }

    /**
     * 修改保存作业
     */
    @RequiresPermissions("homework:leavehomework:edit")
    @Log(title = "作业", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SHomework sHomework)
    {
        return toAjax(sHomeworkService.updateSHomework(sHomework));
    }

    /**
     * 删除作业
     */
    @RequiresPermissions("homework:leavehomework:remove")
    @Log(title = "作业", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(sHomeworkService.deleteSHomeworkByIds(ids));
    }

    //根据作业ID获取作业信息
    @GetMapping("/getHomeWorkInfo")
    @ResponseBody
    public SHomework getHomeWorkInfo(Long id)
    {
        System.err.println("homeworkinfo:"+id);
        return (SHomework) sHomeworkService.selectSHomeworkListById(id);
    }

    /**
     * 查看作业详细
     */
    @RequiresPermissions("homework:leavehomework:detail")
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") Long id, ModelMap mmap)
    {
        mmap.put("sHomework", sHomeworkService.selectSHomeworkById(id));
        return prefix + "/detail";
    }



}
