package com.ruoyi.web.controller.homework;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysRole;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.ShiroUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.homework.domain.SHomework;
import com.ruoyi.homework.domain.SStudentHomework;
import com.ruoyi.homework.service.ISHomeworkService;
import com.ruoyi.homework.service.ISStudentHomeworkService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * 查看作业Controller
 * 
 * @author BahetCoder
 * @date 2021-06-04
 */
@Controller
@RequestMapping("/homework/Stuhomework")
public class SStudentHomeworkController extends BaseController
{
    private String prefix = "homework/Stuhomework";

    @Autowired
    private ISStudentHomeworkService sStudentHomeworkService;


    @Autowired
    private ISHomeworkService sHomeworkService;

    @RequiresPermissions("homework:Stuhomework:view")
    @GetMapping()
    public String Stuhomework()
    {
        return prefix + "/Stuhomework";
    }

    /**
     * 查询查看作业列表
     */
    @RequiresPermissions("homework:Stuhomework:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SStudentHomework sStudentHomework)
    {
        Long userId = ShiroUtils.getUserId();
        SysUser sysUser = ShiroUtils.getSysUser();
        List<SysRole> roles = sysUser.getRoles();
        SysRole sysRole = roles.get(0);
        //教师
        if(sysRole.getRoleId()==3){
            //如果是老师角色只显示当前老师操作的数据
            sStudentHomework.setTeacherId(userId);
        }//学生
        else if(sysRole.getRoleId()==4){
            //如果是学生角色只显示当前学生操作的数据
            sStudentHomework.setStudentId(String.valueOf(userId));
        }

        startPage();
        List<SStudentHomework> list = sStudentHomeworkService.selectSStudentHomeworkList(sStudentHomework);
        return getDataTable(list);
    }

    /**
     * 导出查看作业列表
     */
    @RequiresPermissions("homework:Stuhomework:export")
    @Log(title = "查看作业", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SStudentHomework sStudentHomework)
    {
        List<SStudentHomework> list = sStudentHomeworkService.selectSStudentHomeworkList(sStudentHomework);
        ExcelUtil<SStudentHomework> util = new ExcelUtil<SStudentHomework>(SStudentHomework.class);
        return util.exportExcel(list, "查看作业数据");
    }

    /**
     * 新增查看作业
     */
    @GetMapping("/add/{homeworkId}")
    public String add(@PathVariable(name ="homeworkId" ) Long homeworkId,ModelMap mmap)
    {

        SHomework sHomework = sHomeworkService.selectSHomeworkById(homeworkId);
        mmap.put("sHomework",sHomework);

        return prefix + "/add";
    }

    /**
     * 新增保存查看作业
     */
    @RequiresPermissions("homework:Stuhomework:add")
    @Log(title = "查看作业", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SStudentHomework sStudentHomework)
    {
        //判断作业是否存在
        Long homeworkId = sStudentHomework.getHomeworkId();//当前作业ID
        SStudentHomework ss = sStudentHomeworkService.selectSStudentHomeworkhomeworkIdById(homeworkId);
        if (ss.getId()>0){
            throw  new BusinessException("您已提交作业，不允许重复提交！");
        }

        SHomework sHomework = sHomeworkService.selectSHomeworkById(sStudentHomework.getHomeworkId());
        //判断作业是否截止，如果截止就不让提交
        Date endTime = sHomework.getEndTime();//作业结束时间
        Date nowDate = DateUtils.getNowDate();//当前时间
        if(nowDate.after(endTime)){
            throw  new BusinessException("作业提交时间已截止！");
        }
        return toAjax(sStudentHomeworkService.insertSStudentHomework(sStudentHomework));
    }

    /**
     * 修改查看作业
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        SStudentHomework sStudentHomework = sStudentHomeworkService.selectSStudentHomeworkById(id);
        mmap.put("sStudentHomework", sStudentHomework);
        return prefix + "/edit";
    }

    /**
     * 修改保存查看作业
     */
    @RequiresPermissions("homework:Stuhomework:edit")
    @Log(title = "查看作业", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SStudentHomework sStudentHomework)
    {
        SHomework sHomework = sHomeworkService.selectSHomeworkById(sStudentHomework.getHomeworkId());
        Long homeworkId = sStudentHomework.getHomeworkId();//当前作业ID
        SStudentHomework ss = sStudentHomeworkService.selectSStudentHomeworkhomeworkIdById(homeworkId);
        Integer state = ss.getState();
        if(state ==1){
            throw  new BusinessException("作业已批改，无法再修改！");
        }
        //判断作业是否截止，如果截止就不让提交
        Date endTime = sHomework.getEndTime();//作业结束时间
        Date nowDate = DateUtils.getNowDate();//当前时间

        if(nowDate.after(endTime)){
            throw  new BusinessException("作业提交时间已截止！");
        }
        return toAjax(sStudentHomeworkService.updateSStudentHomework(sStudentHomework));
    }

    /**
     * 作业评分
     */
    @GetMapping("/mark/{id}")
    @RequiresPermissions("homework:Stuhomework:mark")
    public String mark(@PathVariable("id") Long id, ModelMap mmap)
    {
        SStudentHomework sStudentHomework = sStudentHomeworkService.selectSStudentHomeworkById(id);
        mmap.put("sStudentHomework", sStudentHomework);
        return prefix + "/mark";
    }

    /**
     * 删除查看作业
     */
    @RequiresPermissions("homework:Stuhomework:remove")
    @Log(title = "查看作业", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(sStudentHomeworkService.deleteSStudentHomeworkByIds(ids));
    }

    /**
     * 查看作业详细
     */
    @RequiresPermissions("homework:Stuhomework:detail")
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") Long id, ModelMap mmap)
    {
        mmap.put("sStudentHomework", sStudentHomeworkService.selectSStudentHomeworkById(id));
        return prefix + "/detail";
    }
}
