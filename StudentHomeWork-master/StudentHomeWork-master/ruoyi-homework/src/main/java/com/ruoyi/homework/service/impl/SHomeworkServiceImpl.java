package com.ruoyi.homework.service.impl;

import java.util.List;

import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.homework.mapper.SHomeworkMapper;
import com.ruoyi.homework.domain.SHomework;
import com.ruoyi.homework.service.ISHomeworkService;
import com.ruoyi.common.core.text.Convert;

/**
 * 作业Service业务层处理
 * 
 * @author BahetCoder
 * @date 2021-06-04
 */
@Service
public class SHomeworkServiceImpl implements ISHomeworkService 
{
    @Autowired
    private SHomeworkMapper sHomeworkMapper;

    /**
     * 查询作业
     * 
     * @param id 作业ID
     * @return 作业
     */
    @Override
    public SHomework selectSHomeworkById(Long id)
    {
        return sHomeworkMapper.selectSHomeworkById(id);
    }

    /**
     * 查询作业列表
     * 
     * @param sHomework 作业
     * @return 作业
     */
    @Override
    //用户权限注解
    @DataScope(userAlias = "u")
    public List<SHomework> selectSHomeworkList(SHomework sHomework)
    {
        return sHomeworkMapper.selectSHomeworkList(sHomework);
    }

    /**
     * 新增作业
     * 
     * @param sHomework 作业
     * @return 结果
     */
    @Override
    public int insertSHomework(SHomework sHomework)
    {
        sHomework.setCreateTime(DateUtils.getNowDate());//获取当前时间并塞入当前操作的时间
        sHomework.setCreateBy(ShiroUtils.getLoginName());//当前登录用户登录名塞入当前操作数据中
        return sHomeworkMapper.insertSHomework(sHomework);
    }

    /**
     * 修改作业
     * 
     * @param sHomework 作业
     * @return 结果
     */
    @Override
    public int updateSHomework(SHomework sHomework)
    {
        sHomework.setUpdateTime(DateUtils.getNowDate());//获取当前时间并塞入当前操作的时间
        sHomework.setUpdateBy(ShiroUtils.getLoginName());//当前登录用户登录名塞入当前操作数据中
        return sHomeworkMapper.updateSHomework(sHomework);
    }

    /**
     * 删除作业对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSHomeworkByIds(String ids)
    {
        return sHomeworkMapper.deleteSHomeworkByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除作业信息
     * 
     * @param id 作业ID
     * @return 结果
     */
    @Override
    public int deleteSHomeworkById(Long id)
    {
        return sHomeworkMapper.deleteSHomeworkById(id);
    }

    //根据id查询数据
    @Override
    public SHomework selectSHomeworkListById(Long id)
    {
        return  sHomeworkMapper.selectSHomeworkById(id);
    }
}
