package com.ruoyi.homework.service.impl;

import java.util.List;

import com.ruoyi.common.annotation.DataScope;
import com.ruoyi.common.core.domain.entity.SysRole;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.ShiroUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.homework.mapper.SStudentHomeworkMapper;
import com.ruoyi.homework.domain.SStudentHomework;
import com.ruoyi.homework.service.ISStudentHomeworkService;
import com.ruoyi.common.core.text.Convert;

/**
 * 查看作业Service业务层处理
 * 
 * @author BahetCoder
 * @date 2021-06-04
 */
@Service
public class SStudentHomeworkServiceImpl implements ISStudentHomeworkService 
{
    @Autowired
    private SStudentHomeworkMapper sStudentHomeworkMapper;

    /**
     * 查询查看作业
     * 
     * @param id 查看作业ID
     * @return 查看作业
     */
    @Override
    public SStudentHomework selectSStudentHomeworkById(Long id)
    {
        return sStudentHomeworkMapper.selectSStudentHomeworkById(id);
    }

    /**
     * 查询查看作业列表
     * 
     * @param sStudentHomework 查看作业
     * @return 查看作业
     */
    @Override
    //用户权限注解
    @DataScope(userAlias = "u")
    public List<SStudentHomework> selectSStudentHomeworkList(SStudentHomework sStudentHomework)
    {
        return sStudentHomeworkMapper.selectSStudentHomeworkList(sStudentHomework);
    }

    /**
     * 新增查看作业
     * 
     * @param sStudentHomework 查看作业
     * @return 结果
     */
    @Override
    public int insertSStudentHomework(SStudentHomework sStudentHomework)
    {
        sStudentHomework.setCreateTime(DateUtils.getNowDate());//获取当前时间并塞入当前操作的时间
        sStudentHomework.setCreateBy(ShiroUtils.getLoginName());//当前登录用户登录名塞入当前操作数据中
        sStudentHomework.setStuclassid(ShiroUtils.getSysUser().getDeptId());//当前登录用户部门ID塞入当前操作数据中
        sStudentHomework.setStuclassname(ShiroUtils.getSysUser().getDept().getDeptName());//当前登录用户部门名称塞入当前操作数据中
        return sStudentHomeworkMapper.insertSStudentHomework(sStudentHomework);
    }

    /**
     * 修改查看作业
     * 
     * @param sStudentHomework 查看作业
     * @return 结果
     */
    @Override
    public int updateSStudentHomework(SStudentHomework sStudentHomework)
    {
        sStudentHomework.setUpdateTime(DateUtils.getNowDate());//获取当前时间并塞入当前操作的时间
        sStudentHomework.setUpdateBy(ShiroUtils.getLoginName());//当前登录用户登录名塞入当前操作数据中
        SysUser sysUser = ShiroUtils.getSysUser();
        List<SysRole> roles = sysUser.getRoles();
        SysRole sysRole = roles.get(0);
        //教师
        if(sysRole.getRoleId()==3){
            //如果是老师角色批改作业就状态改为已批改作业否则不变
            sStudentHomework.setState(1);
        }
        return sStudentHomeworkMapper.updateSStudentHomework(sStudentHomework);
    }

    /**
     * 删除查看作业对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSStudentHomeworkByIds(String ids)
    {
        return sStudentHomeworkMapper.deleteSStudentHomeworkByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除查看作业信息
     * 
     * @param id 查看作业ID
     * @return 结果
     */
    @Override
    public int deleteSStudentHomeworkById(Long id)
    {
        return sStudentHomeworkMapper.deleteSStudentHomeworkById(id);
    }

    @Override
    public SStudentHomework selectSStudentHomeworkhomeworkIdById(Long homeworkId) {
        return sStudentHomeworkMapper.selectSStudentHomeworkByHomeworkId(homeworkId);
    }
}
