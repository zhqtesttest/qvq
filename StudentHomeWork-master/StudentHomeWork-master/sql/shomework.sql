/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 50727
 Source Host           : 127.0.0.1:3306
 Source Schema         : shomework

 Target Server Type    : MySQL
 Target Server Version : 50727
 File Encoding         : 65001

 Date: 08/06/2021 16:16:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table`  (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作 sub主子表操作）',
  `package_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES (1, 's_homework', '留作业表', '', NULL, 'SHomework', 'crud', 'com.ruoyi.homework', 'homework', 'leavehomework', '作业', 'BahetCoder', '0', '/', '{\"parentMenuId\":\"2000\",\"treeName\":\"\",\"treeParentCode\":\"\",\"parentMenuName\":\"作业管理\",\"treeCode\":\"\"}', 'admin', '2021-06-03 20:55:43', '', '2021-06-04 21:44:34', '');
INSERT INTO `gen_table` VALUES (2, 's_student_homework', '提交作业表', '', NULL, 'SStudentHomework', 'crud', 'com.ruoyi.homework', 'homework', 'Stuhomework', '查看作业', 'BahetCoder', '0', '/', '{\"parentMenuId\":\"2000\",\"treeName\":\"\",\"treeParentCode\":\"\",\"parentMenuName\":\"作业管理\",\"treeCode\":\"\"}', 'admin', '2021-06-03 20:55:43', '', '2021-06-06 14:29:48', '');

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column`  (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '字典类型',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表字段' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES (1, '1', 'id', '主键', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2021-06-03 20:55:43', NULL, '2021-06-04 21:44:34');
INSERT INTO `gen_table_column` VALUES (2, '1', 'teacher_id', '教师id', 'varchar(20)', 'String', 'teacherId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 2, 'admin', '2021-06-03 20:55:43', NULL, '2021-06-04 21:44:34');
INSERT INTO `gen_table_column` VALUES (3, '1', 'teacher_name', '教师姓名', 'varchar(20)', 'String', 'teacherName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', '2021-06-03 20:55:43', NULL, '2021-06-04 21:44:34');
INSERT INTO `gen_table_column` VALUES (4, '1', 'course_name', '课程名称', 'varchar(20)', 'String', 'courseName', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 's_course_name', 4, 'admin', '2021-06-03 20:55:43', NULL, '2021-06-04 21:44:34');
INSERT INTO `gen_table_column` VALUES (5, '1', 'title', '作业标题', 'varchar(100)', 'String', 'title', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2021-06-03 20:55:43', NULL, '2021-06-04 21:44:34');
INSERT INTO `gen_table_column` VALUES (6, '1', 'content', '作业内容', 'varchar(2000)', 'String', 'content', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'summernote', '', 6, 'admin', '2021-06-03 20:55:43', NULL, '2021-06-04 21:44:34');
INSERT INTO `gen_table_column` VALUES (7, '1', 'start_time', '开始时间', 'datetime', 'Date', 'startTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 7, 'admin', '2021-06-03 20:55:43', NULL, '2021-06-04 21:44:34');
INSERT INTO `gen_table_column` VALUES (8, '1', 'end_time', '截止时间', 'datetime', 'Date', 'endTime', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'datetime', '', 8, 'admin', '2021-06-03 20:55:43', NULL, '2021-06-04 21:44:34');
INSERT INTO `gen_table_column` VALUES (9, '1', 'remark', '备注信息', 'varchar(255)', 'String', 'remark', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 9, 'admin', '2021-06-03 20:55:43', NULL, '2021-06-04 21:44:34');
INSERT INTO `gen_table_column` VALUES (10, '1', 'status', '状态(0开始，1结束)', 'char(1)', 'String', 'status', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'select', 's_homework_status', 10, 'admin', '2021-06-03 20:55:43', NULL, '2021-06-04 21:44:34');
INSERT INTO `gen_table_column` VALUES (11, '1', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 11, 'admin', '2021-06-03 20:55:43', NULL, '2021-06-04 21:44:34');
INSERT INTO `gen_table_column` VALUES (12, '1', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 12, 'admin', '2021-06-03 20:55:43', NULL, '2021-06-04 21:44:34');
INSERT INTO `gen_table_column` VALUES (13, '1', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 13, 'admin', '2021-06-03 20:55:43', NULL, '2021-06-04 21:44:34');
INSERT INTO `gen_table_column` VALUES (14, '1', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 14, 'admin', '2021-06-03 20:55:43', NULL, '2021-06-04 21:44:34');
INSERT INTO `gen_table_column` VALUES (15, '2', 'id', '主键', 'bigint(20)', 'Long', 'id', '1', '1', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 1, 'admin', '2021-06-03 20:55:43', NULL, '2021-06-06 14:29:48');
INSERT INTO `gen_table_column` VALUES (16, '2', 'student_id', '学生id', 'varchar(20)', 'String', 'studentId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 2, 'admin', '2021-06-03 20:55:43', NULL, '2021-06-06 14:29:48');
INSERT INTO `gen_table_column` VALUES (17, '2', 'student_name', '学生姓名', 'varchar(20)', 'String', 'studentName', '0', '0', NULL, '1', '1', '1', '1', 'LIKE', 'input', '', 3, 'admin', '2021-06-03 20:55:43', NULL, '2021-06-06 14:29:48');
INSERT INTO `gen_table_column` VALUES (18, '2', 'homework_id', '作业id', 'bigint(20)', 'Long', 'homeworkId', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 4, 'admin', '2021-06-03 20:55:43', NULL, '2021-06-06 14:29:48');
INSERT INTO `gen_table_column` VALUES (19, '2', 'homework_title', '作业标题', 'varchar(100)', 'String', 'homeworkTitle', '0', '0', NULL, '1', '1', '1', '1', 'EQ', 'input', '', 5, 'admin', '2021-06-03 20:55:43', NULL, '2021-06-06 14:29:48');
INSERT INTO `gen_table_column` VALUES (20, '2', 'homework_content', '作业内容', 'varchar(2000)', 'String', 'homeworkContent', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'summernote', '', 6, 'admin', '2021-06-03 20:55:43', NULL, '2021-06-06 14:29:48');
INSERT INTO `gen_table_column` VALUES (21, '2', 'create_time', '提交时间', 'datetime', 'Date', 'createTime', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'datetime', '', 7, 'admin', '2021-06-03 20:55:43', NULL, '2021-06-06 14:29:48');
INSERT INTO `gen_table_column` VALUES (22, '2', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'datetime', '', 8, 'admin', '2021-06-03 20:55:43', NULL, '2021-06-06 14:29:48');
INSERT INTO `gen_table_column` VALUES (23, '2', 'homework_score', '作业评分', 'decimal(11,2)', 'BigDecimal', 'homeworkScore', '0', '0', NULL, '1', '1', '1', NULL, 'EQ', 'input', '', 9, 'admin', '2021-06-03 20:55:43', NULL, '2021-06-06 14:29:48');
INSERT INTO `gen_table_column` VALUES (24, '2', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', NULL, '1', NULL, NULL, NULL, 'EQ', 'input', '', 11, 'admin', '2021-06-03 20:55:43', NULL, '2021-06-06 14:29:48');
INSERT INTO `gen_table_column` VALUES (25, '2', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 12, 'admin', '2021-06-03 20:55:43', NULL, '2021-06-06 14:29:48');
INSERT INTO `gen_table_column` VALUES (26, '2', 'remark', '评分建议', 'varchar(255)', 'String', 'remark', '0', '0', NULL, '1', '1', NULL, NULL, 'EQ', 'input', '', 10, '', '2021-06-06 14:27:39', NULL, '2021-06-06 14:29:48');

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `blob_data` blob,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `calendar` blob NOT NULL,
  PRIMARY KEY (`sched_name`, `calendar_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `cron_expression` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `time_zone_id` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', '0/10 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', '0/15 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', '0/20 * * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `entry_id` varchar(95) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `fired_time` bigint(13) NOT NULL,
  `sched_time` bigint(13) NOT NULL,
  `priority` int(11) NOT NULL,
  `state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `entry_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `job_class_name` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_durable` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_nonconcurrent` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_update_data` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `requests_recovery` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `job_data` blob,
  PRIMARY KEY (`sched_name`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001E636F6D2E72756F79692E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720027636F6D2E72756F79692E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000179D1710A8878707400007070707400013174000E302F3130202A202A202A202A203F74001172795461736B2E72794E6F506172616D7374000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000001740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E697A0E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001E636F6D2E72756F79692E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720027636F6D2E72756F79692E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000179D1710A8878707400007070707400013174000E302F3135202A202A202A202A203F74001572795461736B2E7279506172616D7328277279272974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000002740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E69C89E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001E636F6D2E72756F79692E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720027636F6D2E72756F79692E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000179D1710A8878707400007070707400013174000E302F3230202A202A202A202A203F74003872795461736B2E72794D756C7469706C65506172616D7328277279272C20747275652C20323030304C2C203331362E3530442C203130302974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000003740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E5A49AE58F82EFBC8974000133740001317800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `lock_name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`sched_name`, `lock_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`sched_name`, `trigger_group`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `instance_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `last_checkin_time` bigint(13) NOT NULL,
  `checkin_interval` bigint(13) NOT NULL,
  PRIMARY KEY (`sched_name`, `instance_name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('RuoyiScheduler', 'DESKTOP-JCM6ESO1623139100391', 1623140198152, 15000);

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `repeat_count` bigint(7) NOT NULL,
  `repeat_interval` bigint(12) NOT NULL,
  `times_triggered` bigint(10) NOT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `str_prop_1` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `str_prop_2` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `str_prop_3` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `int_prop_1` int(11) DEFAULT NULL,
  `int_prop_2` int(11) DEFAULT NULL,
  `long_prop_1` bigint(20) DEFAULT NULL,
  `long_prop_2` bigint(20) DEFAULT NULL,
  `dec_prop_1` decimal(13, 4) DEFAULT NULL,
  `dec_prop_2` decimal(13, 4) DEFAULT NULL,
  `bool_prop_1` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `bool_prop_2` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers`  (
  `sched_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `job_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `job_group` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `description` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `next_fire_time` bigint(13) DEFAULT NULL,
  `prev_fire_time` bigint(13) DEFAULT NULL,
  `priority` int(11) DEFAULT NULL,
  `trigger_state` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `trigger_type` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `start_time` bigint(13) NOT NULL,
  `end_time` bigint(13) DEFAULT NULL,
  `calendar_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `misfire_instr` smallint(2) DEFAULT NULL,
  `job_data` blob,
  PRIMARY KEY (`sched_name`, `trigger_name`, `trigger_group`) USING BTREE,
  INDEX `sched_name`(`sched_name`, `job_name`, `job_group`) USING BTREE,
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', 'TASK_CLASS_NAME1', 'DEFAULT', NULL, 1623139100000, -1, 5, 'PAUSED', 'CRON', 1623139100000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', 'TASK_CLASS_NAME2', 'DEFAULT', NULL, 1623139110000, -1, 5, 'PAUSED', 'CRON', 1623139100000, 0, NULL, 2, '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', 'TASK_CLASS_NAME3', 'DEFAULT', NULL, 1623139100000, -1, 5, 'PAUSED', 'CRON', 1623139100000, 0, NULL, 2, '');

-- ----------------------------
-- Table structure for s_homework
-- ----------------------------
DROP TABLE IF EXISTS `s_homework`;
CREATE TABLE `s_homework`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `teacher_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '教师id',
  `teacher_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '教师姓名',
  `course_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '课程名称',
  `stuclassid` bigint(20) DEFAULT NULL COMMENT '班级ID',
  `stuclassname` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '班级名称',
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '作业标题',
  `content` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '作业内容',
  `start_time` datetime(0) DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime(0) DEFAULT NULL COMMENT '截止时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注信息',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '状态(0开始，1结束)',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '更新者',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of s_homework
-- ----------------------------
INSERT INTO `s_homework` VALUES (8, '105', '阿卜', 'yw', 103, 'A班', '背诵阿房宫赋', '<p>背诵阿房宫赋背诵阿房宫赋<br></p>', '2021-06-08 10:06:33', '2021-06-09 18:06:34', '背诵阿房宫赋', '0', '2021-06-08 13:34:16', NULL, 'abu', NULL);
INSERT INTO `s_homework` VALUES (9, '104', '张翰', 'sx', 104, 'B班', '把期末考试模拟卷子写完', '<p>把期末考试模拟卷子写完<br></p>', '2021-06-08 13:06:44', '2021-06-09 18:06:44', '把期末考试模拟卷子写完', '0', '2021-06-08 13:35:58', NULL, 'zh', NULL);

-- ----------------------------
-- Table structure for s_student_homework
-- ----------------------------
DROP TABLE IF EXISTS `s_student_homework`;
CREATE TABLE `s_student_homework`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `student_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '学生id',
  `student_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '学生姓名',
  `homework_id` bigint(20) DEFAULT NULL COMMENT '作业id',
  `homework_title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '作业标题',
  `homework_content` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '作业内容',
  `create_time` datetime(0) DEFAULT NULL COMMENT '提交时间',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `homework_score` decimal(11, 2) DEFAULT 0.00 COMMENT '作业评分',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '评分建议',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '更新者',
  `stuclassid` bigint(20) DEFAULT NULL COMMENT '班级ID',
  `stuclassname` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '班级名称',
  `state` bigint(20) DEFAULT 0 COMMENT '状态(默认0：已完成、1：已平分)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of s_student_homework
-- ----------------------------
INSERT INTO `s_student_homework` VALUES (14, '106', '张三', 8, '背诵阿房宫赋', '<b>背诵阿房宫赋已完成背诵，呵呵呵呵</b>', '2021-06-08 13:36:59', '2021-06-08 16:12:15', 100.00, '100分，背诵的不错呀！', 'zs', 'abu', 103, 'A班', 1);
INSERT INTO `s_student_homework` VALUES (15, '107', '李四', 9, '把期末考试模拟卷子写完', '<p>不会做呀，也不叫我写写，你是干什么吃的老师，呵呵呵</p><p><br></p>', '2021-06-08 13:38:17', '2021-06-08 13:42:03', 0.00, '给零分，什么鬼你这交的！呵呵呵', 'ls', 'zh', 104, 'B班', 0);

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '参数配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2021-06-03 18:35:16', '', NULL, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES (2, '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2021-06-03 18:35:16', '', NULL, '初始化密码 123456');
INSERT INTO `sys_config` VALUES (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2021-06-03 18:35:16', '', NULL, '深黑主题theme-dark，浅色主题theme-light，深蓝主题theme-blue');
INSERT INTO `sys_config` VALUES (4, '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', '2021-06-03 18:35:16', '', NULL, '是否开启注册用户功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES (5, '用户管理-密码字符范围', 'sys.account.chrtype', '0', 'Y', 'admin', '2021-06-03 18:35:16', '', NULL, '默认任意字符范围，0任意（密码可以输入任意字符），1数字（密码只能为0-9数字），2英文字母（密码只能为a-z和A-Z字母），3字母和数字（密码必须包含字母，数字）,4字母数字和特殊字符（目前支持的特殊字符包括：~!@#$%^&*()-=_+）');
INSERT INTO `sys_config` VALUES (6, '用户管理-初始密码修改策略', 'sys.account.initPasswordModify', '0', 'Y', 'admin', '2021-06-03 18:35:16', '', NULL, '0：初始密码修改策略关闭，没有任何提示，1：提醒用户，如果未修改初始密码，则在登录时就会提醒修改密码对话框');
INSERT INTO `sys_config` VALUES (7, '用户管理-账号密码更新周期', 'sys.account.passwordValidateDays', '0', 'Y', 'admin', '2021-06-03 18:35:16', '', NULL, '密码更新周期（填写数字，数据初始化值为0不限制，若修改必须为大于0小于365的正整数），如果超过这个周期登录系统时，则在登录时就会提醒修改密码对话框');
INSERT INTO `sys_config` VALUES (8, '主框架页-菜单导航显示风格', 'sys.index.menuStyle', 'default', 'Y', 'admin', '2021-06-03 18:35:16', '', NULL, '菜单导航显示风格（default为左侧导航菜单，topnav为顶部导航菜单）');
INSERT INTO `sys_config` VALUES (9, '主框架页-是否开启页脚', 'sys.index.ignoreFooter', 'true', 'Y', 'admin', '2021-06-03 18:35:17', '', NULL, '是否开启底部页脚显示（true显示，false隐藏）');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) DEFAULT 0 COMMENT '父部门id',
  `ancestors` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) DEFAULT 0 COMMENT '显示顺序',
  `leader` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '邮箱',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 200 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '部门表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (100, 0, '0', 'XX学校', 0, '阿卜', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-05-31 17:04:18', '', NULL);
INSERT INTO `sys_dept` VALUES (101, 100, '0,100', '信息学院', 1, '阿卜', '13000000000', 'ry@qq.com', '0', '0', 'admin', '2021-05-31 17:04:18', 'admin', '2021-06-01 10:08:16');
INSERT INTO `sys_dept` VALUES (102, 100, '0,100', '长沙分公司', 2, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-05-31 17:04:18', '', NULL);
INSERT INTO `sys_dept` VALUES (103, 101, '0,100,101', 'A班', 1, 'A班主任', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-05-31 17:04:18', 'admin', '2021-06-01 10:08:49');
INSERT INTO `sys_dept` VALUES (104, 101, '0,100,101', 'B班', 2, 'B班主任', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-05-31 17:04:18', 'admin', '2021-06-01 10:09:07');
INSERT INTO `sys_dept` VALUES (105, 101, '0,100,101', 'C班', 3, 'C班主任', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-05-31 17:04:18', 'admin', '2021-06-01 10:13:23');
INSERT INTO `sys_dept` VALUES (106, 101, '0,100,101', 'D班', 4, 'D班主任', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-05-31 17:04:18', 'admin', '2021-06-01 10:13:35');
INSERT INTO `sys_dept` VALUES (107, 101, '0,100,101', 'E班', 5, 'E班主任', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2021-05-31 17:04:18', 'admin', '2021-06-01 10:16:24');
INSERT INTO `sys_dept` VALUES (108, 102, '0,100,102', '市场部门', 1, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-05-31 17:04:18', '', NULL);
INSERT INTO `sys_dept` VALUES (109, 102, '0,100,102', '财务部门', 2, '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2021-05-31 17:04:18', '', NULL);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) DEFAULT 0 COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 106 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典数据表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2021-06-03 18:35:15', '', NULL, '性别男');
INSERT INTO `sys_dict_data` VALUES (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2021-06-03 18:35:15', '', NULL, '性别女');
INSERT INTO `sys_dict_data` VALUES (3, 3, '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2021-06-03 18:35:15', '', NULL, '性别未知');
INSERT INTO `sys_dict_data` VALUES (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2021-06-03 18:35:15', '', NULL, '显示菜单');
INSERT INTO `sys_dict_data` VALUES (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2021-06-03 18:35:15', '', NULL, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2021-06-03 18:35:15', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2021-06-03 18:35:15', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2021-06-03 18:35:15', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2021-06-03 18:35:15', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2021-06-03 18:35:15', '', NULL, '默认分组');
INSERT INTO `sys_dict_data` VALUES (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2021-06-03 18:35:15', '', NULL, '系统分组');
INSERT INTO `sys_dict_data` VALUES (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2021-06-03 18:35:15', '', NULL, '系统默认是');
INSERT INTO `sys_dict_data` VALUES (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2021-06-03 18:35:15', '', NULL, '系统默认否');
INSERT INTO `sys_dict_data` VALUES (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2021-06-03 18:35:15', '', NULL, '通知');
INSERT INTO `sys_dict_data` VALUES (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2021-06-03 18:35:15', '', NULL, '公告');
INSERT INTO `sys_dict_data` VALUES (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2021-06-03 18:35:15', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2021-06-03 18:35:15', '', NULL, '关闭状态');
INSERT INTO `sys_dict_data` VALUES (18, 99, '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-06-03 18:35:15', '', NULL, '其他操作');
INSERT INTO `sys_dict_data` VALUES (19, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-06-03 18:35:16', '', NULL, '新增操作');
INSERT INTO `sys_dict_data` VALUES (20, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2021-06-03 18:35:16', '', NULL, '修改操作');
INSERT INTO `sys_dict_data` VALUES (21, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-06-03 18:35:16', '', NULL, '删除操作');
INSERT INTO `sys_dict_data` VALUES (22, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2021-06-03 18:35:16', '', NULL, '授权操作');
INSERT INTO `sys_dict_data` VALUES (23, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-06-03 18:35:16', '', NULL, '导出操作');
INSERT INTO `sys_dict_data` VALUES (24, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-06-03 18:35:16', '', NULL, '导入操作');
INSERT INTO `sys_dict_data` VALUES (25, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-06-03 18:35:16', '', NULL, '强退操作');
INSERT INTO `sys_dict_data` VALUES (26, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2021-06-03 18:35:16', '', NULL, '生成操作');
INSERT INTO `sys_dict_data` VALUES (27, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2021-06-03 18:35:16', '', NULL, '清空操作');
INSERT INTO `sys_dict_data` VALUES (28, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2021-06-03 18:35:16', '', NULL, '正常状态');
INSERT INTO `sys_dict_data` VALUES (29, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2021-06-03 18:35:16', '', NULL, '停用状态');
INSERT INTO `sys_dict_data` VALUES (30, 1, '语文', 'yw', 's_course_name', NULL, NULL, 'Y', '0', 'admin', '2021-06-01 09:47:11', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (31, 2, '数学', 'sx', 's_course_name', NULL, NULL, 'Y', '0', 'admin', '2021-06-01 09:47:26', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (32, 3, '外语', 'wy', 's_course_name', NULL, NULL, 'Y', '0', 'admin', '2021-06-01 09:47:39', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (33, 1, '开始', '0', 's_homework_status', '', 'info', 'Y', '0', 'admin', '2021-06-01 10:24:48', 'admin', '2021-06-01 10:25:26', '作业开始');
INSERT INTO `sys_dict_data` VALUES (34, 2, '结束', '1', 's_homework_status', '', 'danger', 'Y', '0', 'admin', '2021-06-01 10:25:08', 'admin', '2021-06-01 10:25:36', '作业结束');
INSERT INTO `sys_dict_data` VALUES (104, 1, '已完成', '0', 'dohomework_statues', NULL, 'success', 'Y', '0', 'admin', '2021-06-08 14:13:41', '', NULL, NULL);
INSERT INTO `sys_dict_data` VALUES (105, 2, '已批改', '1', 'dohomework_statues', NULL, 'primary', 'Y', '0', 'admin', '2021-06-08 14:14:07', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 101 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典类型表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '用户性别', 'sys_user_sex', '0', 'admin', '2021-06-03 18:35:14', '', NULL, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2021-06-03 18:35:14', '', NULL, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2021-06-03 18:35:14', '', NULL, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES (4, '任务状态', 'sys_job_status', '0', 'admin', '2021-06-03 18:35:14', '', NULL, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES (5, '任务分组', 'sys_job_group', '0', 'admin', '2021-06-03 18:35:14', '', NULL, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES (6, '系统是否', 'sys_yes_no', '0', 'admin', '2021-06-03 18:35:14', '', NULL, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES (7, '通知类型', 'sys_notice_type', '0', 'admin', '2021-06-03 18:35:14', '', NULL, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES (8, '通知状态', 'sys_notice_status', '0', 'admin', '2021-06-03 18:35:14', '', NULL, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES (9, '操作类型', 'sys_oper_type', '0', 'admin', '2021-06-03 18:35:15', '', NULL, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES (10, '系统状态', 'sys_common_status', '0', 'admin', '2021-06-03 18:35:15', '', NULL, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES (11, '课程名称', 's_course_name', '0', 'admin', '2021-06-01 09:46:06', '', NULL, '课程名称');
INSERT INTO `sys_dict_type` VALUES (12, '作业状态', 's_homework_status', '0', 'admin', '2021-06-01 10:24:23', '', NULL, '作业状态');
INSERT INTO `sys_dict_type` VALUES (100, '作业完成状态', 'dohomework_statues', '0', 'admin', '2021-06-08 14:13:02', '', NULL, '学生作业完成状态');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`, `job_name`, `job_group`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2021-06-03 18:35:17', '', NULL, '');
INSERT INTO `sys_job` VALUES (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2021-06-03 18:35:17', '', NULL, '');
INSERT INTO `sys_job` VALUES (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2021-06-03 18:35:17', '', NULL, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '日志信息',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '异常信息',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor`  (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `login_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '登录账号',
  `ipaddr` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '操作系统',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '提示消息',
  `login_time` datetime(0) DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 296 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES (100, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-03 18:37:39');
INSERT INTO `sys_logininfor` VALUES (101, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-03 19:04:48');
INSERT INTO `sys_logininfor` VALUES (102, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-04 21:31:52');
INSERT INTO `sys_logininfor` VALUES (103, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-04 22:20:45');
INSERT INTO `sys_logininfor` VALUES (104, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-04 22:44:09');
INSERT INTO `sys_logininfor` VALUES (105, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-04 22:54:14');
INSERT INTO `sys_logininfor` VALUES (106, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-04 22:54:17');
INSERT INTO `sys_logininfor` VALUES (107, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-06-06 14:27:19');
INSERT INTO `sys_logininfor` VALUES (108, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-06-06 15:08:51');
INSERT INTO `sys_logininfor` VALUES (109, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-06-06 15:09:53');
INSERT INTO `sys_logininfor` VALUES (110, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-06-06 15:17:20');
INSERT INTO `sys_logininfor` VALUES (111, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-06-06 15:18:01');
INSERT INTO `sys_logininfor` VALUES (112, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-06-06 16:49:49');
INSERT INTO `sys_logininfor` VALUES (113, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-06-06 17:55:47');
INSERT INTO `sys_logininfor` VALUES (114, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-06-06 18:52:38');
INSERT INTO `sys_logininfor` VALUES (115, 'admin', '127.0.0.1', '内网IP', 'Chrome', 'Windows 10', '0', '登录成功', '2021-06-06 20:56:18');
INSERT INTO `sys_logininfor` VALUES (116, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 13:53:24');
INSERT INTO `sys_logininfor` VALUES (117, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 14:02:34');
INSERT INTO `sys_logininfor` VALUES (118, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 14:02:37');
INSERT INTO `sys_logininfor` VALUES (119, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 14:10:25');
INSERT INTO `sys_logininfor` VALUES (120, 'abu', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 14:10:34');
INSERT INTO `sys_logininfor` VALUES (121, 'abu', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 14:10:42');
INSERT INTO `sys_logininfor` VALUES (122, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 14:10:45');
INSERT INTO `sys_logininfor` VALUES (123, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 14:13:01');
INSERT INTO `sys_logininfor` VALUES (124, 'abu', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 14:13:11');
INSERT INTO `sys_logininfor` VALUES (125, 'abu', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 14:15:16');
INSERT INTO `sys_logininfor` VALUES (126, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 14:15:20');
INSERT INTO `sys_logininfor` VALUES (127, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 14:26:13');
INSERT INTO `sys_logininfor` VALUES (128, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 14:26:40');
INSERT INTO `sys_logininfor` VALUES (129, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 14:26:43');
INSERT INTO `sys_logininfor` VALUES (130, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 14:26:46');
INSERT INTO `sys_logininfor` VALUES (131, 'abu', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 14:26:58');
INSERT INTO `sys_logininfor` VALUES (132, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 14:30:33');
INSERT INTO `sys_logininfor` VALUES (133, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 14:33:53');
INSERT INTO `sys_logininfor` VALUES (134, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 14:33:57');
INSERT INTO `sys_logininfor` VALUES (135, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 14:34:56');
INSERT INTO `sys_logininfor` VALUES (136, 'abu', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 14:35:07');
INSERT INTO `sys_logininfor` VALUES (137, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 16:55:47');
INSERT INTO `sys_logininfor` VALUES (138, 'ABU', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 17:45:51');
INSERT INTO `sys_logininfor` VALUES (139, 'ABU', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 17:45:55');
INSERT INTO `sys_logininfor` VALUES (140, 'abu', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 17:49:45');
INSERT INTO `sys_logininfor` VALUES (141, 'abu', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 17:53:34');
INSERT INTO `sys_logininfor` VALUES (142, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 18:30:19');
INSERT INTO `sys_logininfor` VALUES (143, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 18:34:12');
INSERT INTO `sys_logininfor` VALUES (144, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 18:35:12');
INSERT INTO `sys_logininfor` VALUES (145, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 18:35:44');
INSERT INTO `sys_logininfor` VALUES (146, 'abu', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 18:35:51');
INSERT INTO `sys_logininfor` VALUES (147, 'abu', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 18:50:03');
INSERT INTO `sys_logininfor` VALUES (148, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 18:50:08');
INSERT INTO `sys_logininfor` VALUES (149, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 18:58:36');
INSERT INTO `sys_logininfor` VALUES (150, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 18:58:45');
INSERT INTO `sys_logininfor` VALUES (151, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 19:02:37');
INSERT INTO `sys_logininfor` VALUES (152, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 19:02:48');
INSERT INTO `sys_logininfor` VALUES (153, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 19:02:51');
INSERT INTO `sys_logininfor` VALUES (154, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 19:47:50');
INSERT INTO `sys_logininfor` VALUES (155, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 19:47:52');
INSERT INTO `sys_logininfor` VALUES (156, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 19:47:55');
INSERT INTO `sys_logininfor` VALUES (157, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 19:47:58');
INSERT INTO `sys_logininfor` VALUES (158, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 19:48:02');
INSERT INTO `sys_logininfor` VALUES (159, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 19:48:28');
INSERT INTO `sys_logininfor` VALUES (160, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 19:48:34');
INSERT INTO `sys_logininfor` VALUES (161, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 19:53:29');
INSERT INTO `sys_logininfor` VALUES (162, 'abu', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 19:53:39');
INSERT INTO `sys_logininfor` VALUES (163, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 20:03:15');
INSERT INTO `sys_logininfor` VALUES (164, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 20:03:20');
INSERT INTO `sys_logininfor` VALUES (165, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 20:03:36');
INSERT INTO `sys_logininfor` VALUES (166, 'abu', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '密码输入错误1次', '2021-06-07 20:03:43');
INSERT INTO `sys_logininfor` VALUES (167, 'abu', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 20:03:56');
INSERT INTO `sys_logininfor` VALUES (168, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 20:08:28');
INSERT INTO `sys_logininfor` VALUES (169, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 20:10:28');
INSERT INTO `sys_logininfor` VALUES (170, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 20:12:41');
INSERT INTO `sys_logininfor` VALUES (171, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 20:13:38');
INSERT INTO `sys_logininfor` VALUES (172, 'abu', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 20:13:47');
INSERT INTO `sys_logininfor` VALUES (173, 'abu', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 20:26:53');
INSERT INTO `sys_logininfor` VALUES (174, 'abu', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 20:28:01');
INSERT INTO `sys_logininfor` VALUES (175, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 20:35:26');
INSERT INTO `sys_logininfor` VALUES (176, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 20:35:57');
INSERT INTO `sys_logininfor` VALUES (177, 'abu', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 20:36:14');
INSERT INTO `sys_logininfor` VALUES (178, 'abu', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 20:38:09');
INSERT INTO `sys_logininfor` VALUES (179, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 20:38:13');
INSERT INTO `sys_logininfor` VALUES (180, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 20:38:25');
INSERT INTO `sys_logininfor` VALUES (181, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 20:38:36');
INSERT INTO `sys_logininfor` VALUES (182, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 20:39:39');
INSERT INTO `sys_logininfor` VALUES (183, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 20:39:42');
INSERT INTO `sys_logininfor` VALUES (184, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 20:39:50');
INSERT INTO `sys_logininfor` VALUES (185, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 20:40:11');
INSERT INTO `sys_logininfor` VALUES (186, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 20:40:25');
INSERT INTO `sys_logininfor` VALUES (187, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-07 20:40:36');
INSERT INTO `sys_logininfor` VALUES (188, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 20:40:42');
INSERT INTO `sys_logininfor` VALUES (189, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 20:40:49');
INSERT INTO `sys_logininfor` VALUES (190, 'zs', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-07 20:41:40');
INSERT INTO `sys_logininfor` VALUES (191, 'zs', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2021-06-07 20:46:07');
INSERT INTO `sys_logininfor` VALUES (192, 'zs', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-07 20:46:10');
INSERT INTO `sys_logininfor` VALUES (193, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 20:48:27');
INSERT INTO `sys_logininfor` VALUES (194, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 20:48:32');
INSERT INTO `sys_logininfor` VALUES (195, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 20:48:35');
INSERT INTO `sys_logininfor` VALUES (196, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-07 20:54:59');
INSERT INTO `sys_logininfor` VALUES (197, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 21:04:19');
INSERT INTO `sys_logininfor` VALUES (198, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 21:05:56');
INSERT INTO `sys_logininfor` VALUES (199, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 21:19:01');
INSERT INTO `sys_logininfor` VALUES (200, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 21:24:00');
INSERT INTO `sys_logininfor` VALUES (201, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 21:25:34');
INSERT INTO `sys_logininfor` VALUES (202, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 21:28:20');
INSERT INTO `sys_logininfor` VALUES (203, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 21:31:01');
INSERT INTO `sys_logininfor` VALUES (204, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 21:33:42');
INSERT INTO `sys_logininfor` VALUES (205, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-07 21:38:48');
INSERT INTO `sys_logininfor` VALUES (206, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 21:38:51');
INSERT INTO `sys_logininfor` VALUES (207, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-07 21:39:47');
INSERT INTO `sys_logininfor` VALUES (208, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2021-06-08 08:40:16');
INSERT INTO `sys_logininfor` VALUES (209, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 08:40:18');
INSERT INTO `sys_logininfor` VALUES (210, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 09:05:46');
INSERT INTO `sys_logininfor` VALUES (211, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 09:22:00');
INSERT INTO `sys_logininfor` VALUES (212, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2021-06-08 09:22:02');
INSERT INTO `sys_logininfor` VALUES (213, 'zs', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 09:22:11');
INSERT INTO `sys_logininfor` VALUES (214, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 09:31:12');
INSERT INTO `sys_logininfor` VALUES (215, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 09:33:33');
INSERT INTO `sys_logininfor` VALUES (216, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2021-06-08 09:33:36');
INSERT INTO `sys_logininfor` VALUES (217, 'zs', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 09:33:43');
INSERT INTO `sys_logininfor` VALUES (218, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2021-06-08 09:36:46');
INSERT INTO `sys_logininfor` VALUES (219, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2021-06-08 09:36:49');
INSERT INTO `sys_logininfor` VALUES (220, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 09:36:53');
INSERT INTO `sys_logininfor` VALUES (221, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2021-06-08 09:36:56');
INSERT INTO `sys_logininfor` VALUES (222, 'zs', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 09:37:15');
INSERT INTO `sys_logininfor` VALUES (223, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 09:40:37');
INSERT INTO `sys_logininfor` VALUES (224, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2021-06-08 09:40:41');
INSERT INTO `sys_logininfor` VALUES (225, 'zs', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 09:40:47');
INSERT INTO `sys_logininfor` VALUES (226, 'zs', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 09:49:17');
INSERT INTO `sys_logininfor` VALUES (227, 'zs', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 09:52:20');
INSERT INTO `sys_logininfor` VALUES (228, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 09:58:13');
INSERT INTO `sys_logininfor` VALUES (229, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 10:19:32');
INSERT INTO `sys_logininfor` VALUES (230, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 10:21:04');
INSERT INTO `sys_logininfor` VALUES (231, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 10:57:47');
INSERT INTO `sys_logininfor` VALUES (232, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 12:02:35');
INSERT INTO `sys_logininfor` VALUES (233, 'zh', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 12:26:38');
INSERT INTO `sys_logininfor` VALUES (234, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 12:45:07');
INSERT INTO `sys_logininfor` VALUES (235, 'zh', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2021-06-08 12:45:30');
INSERT INTO `sys_logininfor` VALUES (236, 'zh', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 12:45:33');
INSERT INTO `sys_logininfor` VALUES (237, 'zh', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2021-06-08 12:47:01');
INSERT INTO `sys_logininfor` VALUES (238, 'zh', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 12:47:03');
INSERT INTO `sys_logininfor` VALUES (239, 'zh', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 12:48:29');
INSERT INTO `sys_logininfor` VALUES (240, '阿卜', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '用户不存在/密码错误', '2021-06-08 12:51:20');
INSERT INTO `sys_logininfor` VALUES (241, 'abu', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 12:51:31');
INSERT INTO `sys_logininfor` VALUES (242, 'abu', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-08 12:52:33');
INSERT INTO `sys_logininfor` VALUES (243, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 12:52:44');
INSERT INTO `sys_logininfor` VALUES (244, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-08 12:55:50');
INSERT INTO `sys_logininfor` VALUES (245, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 12:55:53');
INSERT INTO `sys_logininfor` VALUES (246, 'zh', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2021-06-08 13:01:57');
INSERT INTO `sys_logininfor` VALUES (247, 'abu', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 13:02:07');
INSERT INTO `sys_logininfor` VALUES (248, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-08 13:04:04');
INSERT INTO `sys_logininfor` VALUES (249, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 13:04:12');
INSERT INTO `sys_logininfor` VALUES (250, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-08 13:04:46');
INSERT INTO `sys_logininfor` VALUES (251, 'wm', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 13:04:55');
INSERT INTO `sys_logininfor` VALUES (252, 'abu', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 13:17:15');
INSERT INTO `sys_logininfor` VALUES (253, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-08 13:22:48');
INSERT INTO `sys_logininfor` VALUES (254, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 13:22:51');
INSERT INTO `sys_logininfor` VALUES (255, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-08 13:28:57');
INSERT INTO `sys_logininfor` VALUES (256, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 13:29:02');
INSERT INTO `sys_logininfor` VALUES (257, 'abu', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2021-06-08 13:33:22');
INSERT INTO `sys_logininfor` VALUES (258, 'abu', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 13:33:29');
INSERT INTO `sys_logininfor` VALUES (259, 'abu', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2021-06-08 13:34:23');
INSERT INTO `sys_logininfor` VALUES (260, 'zh', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 13:34:33');
INSERT INTO `sys_logininfor` VALUES (261, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-08 13:36:08');
INSERT INTO `sys_logininfor` VALUES (262, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 13:36:16');
INSERT INTO `sys_logininfor` VALUES (263, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-08 13:37:32');
INSERT INTO `sys_logininfor` VALUES (264, 'ls', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 13:37:44');
INSERT INTO `sys_logininfor` VALUES (265, 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 13:39:40');
INSERT INTO `sys_logininfor` VALUES (266, 'zh', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2021-06-08 13:43:39');
INSERT INTO `sys_logininfor` VALUES (267, 'abu', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 13:43:48');
INSERT INTO `sys_logininfor` VALUES (268, 'abu', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2021-06-08 14:09:05');
INSERT INTO `sys_logininfor` VALUES (269, 'abu', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 14:09:08');
INSERT INTO `sys_logininfor` VALUES (270, 'abu', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2021-06-08 14:10:20');
INSERT INTO `sys_logininfor` VALUES (271, 'abu', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 14:10:22');
INSERT INTO `sys_logininfor` VALUES (272, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 14:12:20');
INSERT INTO `sys_logininfor` VALUES (273, 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '退出成功', '2021-06-08 14:17:24');
INSERT INTO `sys_logininfor` VALUES (274, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 14:17:33');
INSERT INTO `sys_logininfor` VALUES (275, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 14:20:15');
INSERT INTO `sys_logininfor` VALUES (276, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 14:21:28');
INSERT INTO `sys_logininfor` VALUES (277, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 14:28:19');
INSERT INTO `sys_logininfor` VALUES (278, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 14:31:32');
INSERT INTO `sys_logininfor` VALUES (279, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 14:34:48');
INSERT INTO `sys_logininfor` VALUES (280, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 14:43:09');
INSERT INTO `sys_logininfor` VALUES (281, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 14:46:10');
INSERT INTO `sys_logininfor` VALUES (282, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 14:51:07');
INSERT INTO `sys_logininfor` VALUES (283, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 14:53:42');
INSERT INTO `sys_logininfor` VALUES (284, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 14:59:57');
INSERT INTO `sys_logininfor` VALUES (285, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-08 15:06:13');
INSERT INTO `sys_logininfor` VALUES (286, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 15:06:17');
INSERT INTO `sys_logininfor` VALUES (287, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 15:31:44');
INSERT INTO `sys_logininfor` VALUES (288, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '1', '验证码错误', '2021-06-08 15:36:00');
INSERT INTO `sys_logininfor` VALUES (289, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 15:36:02');
INSERT INTO `sys_logininfor` VALUES (290, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 15:46:47');
INSERT INTO `sys_logininfor` VALUES (291, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 15:49:07');
INSERT INTO `sys_logininfor` VALUES (292, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 15:50:26');
INSERT INTO `sys_logininfor` VALUES (293, 'zs', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', '0', '登录成功', '2021-06-08 16:00:22');
INSERT INTO `sys_logininfor` VALUES (294, 'abu', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2021-06-08 16:06:01');
INSERT INTO `sys_logininfor` VALUES (295, 'abu', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2021-06-08 16:06:03');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) DEFAULT 0 COMMENT '父菜单ID',
  `order_num` int(4) DEFAULT 0 COMMENT '显示顺序',
  `url` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '#' COMMENT '请求地址',
  `target` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '打开方式（menuItem页签 menuBlank新窗口）',
  `menu_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `is_refresh` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '1' COMMENT '是否刷新（0刷新 1不刷新）',
  `perms` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2018 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '菜单权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 1, '#', '', 'M', '0', '1', '', 'fa fa-gear', 'admin', '2021-06-03 18:35:06', '', NULL, '系统管理目录');
INSERT INTO `sys_menu` VALUES (2, '系统监控', 0, 2, '#', '', 'M', '0', '1', '', 'fa fa-video-camera', 'admin', '2021-06-03 18:35:06', '', NULL, '系统监控目录');
INSERT INTO `sys_menu` VALUES (3, '系统工具', 0, 3, '#', '', 'M', '0', '1', '', 'fa fa-bars', 'admin', '2021-06-03 18:35:06', '', NULL, '系统工具目录');
INSERT INTO `sys_menu` VALUES (100, '用户管理', 1, 1, '/system/user', '', 'C', '0', '1', 'system:user:view', 'fa fa-user-o', 'admin', '2021-06-03 18:35:06', '', NULL, '用户管理菜单');
INSERT INTO `sys_menu` VALUES (101, '角色管理', 1, 2, '/system/role', '', 'C', '0', '1', 'system:role:view', 'fa fa-user-secret', 'admin', '2021-06-03 18:35:06', '', NULL, '角色管理菜单');
INSERT INTO `sys_menu` VALUES (102, '菜单管理', 1, 3, '/system/menu', '', 'C', '0', '1', 'system:menu:view', 'fa fa-th-list', 'admin', '2021-06-03 18:35:06', '', NULL, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES (103, '部门管理', 1, 4, '/system/dept', '', 'C', '0', '1', 'system:dept:view', 'fa fa-outdent', 'admin', '2021-06-03 18:35:06', '', NULL, '部门管理菜单');
INSERT INTO `sys_menu` VALUES (104, '岗位管理', 1, 5, '/system/post', '', 'C', '0', '1', 'system:post:view', 'fa fa-address-card-o', 'admin', '2021-06-03 18:35:06', '', NULL, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES (105, '字典管理', 1, 6, '/system/dict', '', 'C', '0', '1', 'system:dict:view', 'fa fa-bookmark-o', 'admin', '2021-06-03 18:35:06', '', NULL, '字典管理菜单');
INSERT INTO `sys_menu` VALUES (106, '参数设置', 1, 7, '/system/config', '', 'C', '0', '1', 'system:config:view', 'fa fa-sun-o', 'admin', '2021-06-03 18:35:07', '', NULL, '参数设置菜单');
INSERT INTO `sys_menu` VALUES (107, '通知公告', 1, 8, '/system/notice', '', 'C', '0', '1', 'system:notice:view', 'fa fa-bullhorn', 'admin', '2021-06-03 18:35:07', '', NULL, '通知公告菜单');
INSERT INTO `sys_menu` VALUES (108, '日志管理', 1, 9, '#', '', 'M', '0', '1', '', 'fa fa-pencil-square-o', 'admin', '2021-06-03 18:35:07', '', NULL, '日志管理菜单');
INSERT INTO `sys_menu` VALUES (109, '在线用户', 2, 1, '/monitor/online', '', 'C', '0', '1', 'monitor:online:view', 'fa fa-user-circle', 'admin', '2021-06-03 18:35:07', '', NULL, '在线用户菜单');
INSERT INTO `sys_menu` VALUES (110, '定时任务', 2, 2, '/monitor/job', '', 'C', '0', '1', 'monitor:job:view', 'fa fa-tasks', 'admin', '2021-06-03 18:35:07', '', NULL, '定时任务菜单');
INSERT INTO `sys_menu` VALUES (111, '数据监控', 2, 3, '/monitor/data', '', 'C', '0', '1', 'monitor:data:view', 'fa fa-bug', 'admin', '2021-06-03 18:35:07', '', NULL, '数据监控菜单');
INSERT INTO `sys_menu` VALUES (112, '服务监控', 2, 4, '/monitor/server', '', 'C', '0', '1', 'monitor:server:view', 'fa fa-server', 'admin', '2021-06-03 18:35:07', '', NULL, '服务监控菜单');
INSERT INTO `sys_menu` VALUES (113, '缓存监控', 2, 5, '/monitor/cache', '', 'C', '0', '1', 'monitor:cache:view', 'fa fa-cube', 'admin', '2021-06-03 18:35:07', '', NULL, '缓存监控菜单');
INSERT INTO `sys_menu` VALUES (114, '表单构建', 3, 1, '/tool/build', '', 'C', '0', '1', 'tool:build:view', 'fa fa-wpforms', 'admin', '2021-06-03 18:35:07', '', NULL, '表单构建菜单');
INSERT INTO `sys_menu` VALUES (115, '代码生成', 3, 2, '/tool/gen', '', 'C', '0', '1', 'tool:gen:view', 'fa fa-code', 'admin', '2021-06-03 18:35:07', '', NULL, '代码生成菜单');
INSERT INTO `sys_menu` VALUES (116, '系统接口', 3, 3, '/tool/swagger', '', 'C', '0', '1', 'tool:swagger:view', 'fa fa-gg', 'admin', '2021-06-03 18:35:07', '', NULL, '系统接口菜单');
INSERT INTO `sys_menu` VALUES (500, '操作日志', 108, 1, '/monitor/operlog', '', 'C', '0', '1', 'monitor:operlog:view', 'fa fa-address-book', 'admin', '2021-06-03 18:35:07', '', NULL, '操作日志菜单');
INSERT INTO `sys_menu` VALUES (501, '登录日志', 108, 2, '/monitor/logininfor', '', 'C', '0', '1', 'monitor:logininfor:view', 'fa fa-file-image-o', 'admin', '2021-06-03 18:35:07', '', NULL, '登录日志菜单');
INSERT INTO `sys_menu` VALUES (1000, '用户查询', 100, 1, '#', '', 'F', '0', '1', 'system:user:list', '#', 'admin', '2021-06-03 18:35:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1001, '用户新增', 100, 2, '#', '', 'F', '0', '1', 'system:user:add', '#', 'admin', '2021-06-03 18:35:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1002, '用户修改', 100, 3, '#', '', 'F', '0', '1', 'system:user:edit', '#', 'admin', '2021-06-03 18:35:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1003, '用户删除', 100, 4, '#', '', 'F', '0', '1', 'system:user:remove', '#', 'admin', '2021-06-03 18:35:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1004, '用户导出', 100, 5, '#', '', 'F', '0', '1', 'system:user:export', '#', 'admin', '2021-06-03 18:35:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1005, '用户导入', 100, 6, '#', '', 'F', '0', '1', 'system:user:import', '#', 'admin', '2021-06-03 18:35:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1006, '重置密码', 100, 7, '#', '', 'F', '0', '1', 'system:user:resetPwd', '#', 'admin', '2021-06-03 18:35:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1007, '角色查询', 101, 1, '#', '', 'F', '0', '1', 'system:role:list', '#', 'admin', '2021-06-03 18:35:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1008, '角色新增', 101, 2, '#', '', 'F', '0', '1', 'system:role:add', '#', 'admin', '2021-06-03 18:35:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1009, '角色修改', 101, 3, '#', '', 'F', '0', '1', 'system:role:edit', '#', 'admin', '2021-06-03 18:35:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1010, '角色删除', 101, 4, '#', '', 'F', '0', '1', 'system:role:remove', '#', 'admin', '2021-06-03 18:35:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1011, '角色导出', 101, 5, '#', '', 'F', '0', '1', 'system:role:export', '#', 'admin', '2021-06-03 18:35:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1012, '菜单查询', 102, 1, '#', '', 'F', '0', '1', 'system:menu:list', '#', 'admin', '2021-06-03 18:35:07', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1013, '菜单新增', 102, 2, '#', '', 'F', '0', '1', 'system:menu:add', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1014, '菜单修改', 102, 3, '#', '', 'F', '0', '1', 'system:menu:edit', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1015, '菜单删除', 102, 4, '#', '', 'F', '0', '1', 'system:menu:remove', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1016, '部门查询', 103, 1, '#', '', 'F', '0', '1', 'system:dept:list', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1017, '部门新增', 103, 2, '#', '', 'F', '0', '1', 'system:dept:add', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1018, '部门修改', 103, 3, '#', '', 'F', '0', '1', 'system:dept:edit', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1019, '部门删除', 103, 4, '#', '', 'F', '0', '1', 'system:dept:remove', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1020, '岗位查询', 104, 1, '#', '', 'F', '0', '1', 'system:post:list', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1021, '岗位新增', 104, 2, '#', '', 'F', '0', '1', 'system:post:add', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1022, '岗位修改', 104, 3, '#', '', 'F', '0', '1', 'system:post:edit', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1023, '岗位删除', 104, 4, '#', '', 'F', '0', '1', 'system:post:remove', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1024, '岗位导出', 104, 5, '#', '', 'F', '0', '1', 'system:post:export', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1025, '字典查询', 105, 1, '#', '', 'F', '0', '1', 'system:dict:list', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1026, '字典新增', 105, 2, '#', '', 'F', '0', '1', 'system:dict:add', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1027, '字典修改', 105, 3, '#', '', 'F', '0', '1', 'system:dict:edit', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1028, '字典删除', 105, 4, '#', '', 'F', '0', '1', 'system:dict:remove', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1029, '字典导出', 105, 5, '#', '', 'F', '0', '1', 'system:dict:export', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1030, '参数查询', 106, 1, '#', '', 'F', '0', '1', 'system:config:list', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1031, '参数新增', 106, 2, '#', '', 'F', '0', '1', 'system:config:add', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1032, '参数修改', 106, 3, '#', '', 'F', '0', '1', 'system:config:edit', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1033, '参数删除', 106, 4, '#', '', 'F', '0', '1', 'system:config:remove', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1034, '参数导出', 106, 5, '#', '', 'F', '0', '1', 'system:config:export', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1035, '公告查询', 107, 1, '#', '', 'F', '0', '1', 'system:notice:list', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1036, '公告新增', 107, 2, '#', '', 'F', '0', '1', 'system:notice:add', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1037, '公告修改', 107, 3, '#', '', 'F', '0', '1', 'system:notice:edit', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1038, '公告删除', 107, 4, '#', '', 'F', '0', '1', 'system:notice:remove', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1039, '操作查询', 500, 1, '#', '', 'F', '0', '1', 'monitor:operlog:list', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1040, '操作删除', 500, 2, '#', '', 'F', '0', '1', 'monitor:operlog:remove', '#', 'admin', '2021-06-03 18:35:08', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1041, '详细信息', 500, 3, '#', '', 'F', '0', '1', 'monitor:operlog:detail', '#', 'admin', '2021-06-03 18:35:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1042, '日志导出', 500, 4, '#', '', 'F', '0', '1', 'monitor:operlog:export', '#', 'admin', '2021-06-03 18:35:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1043, '登录查询', 501, 1, '#', '', 'F', '0', '1', 'monitor:logininfor:list', '#', 'admin', '2021-06-03 18:35:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1044, '登录删除', 501, 2, '#', '', 'F', '0', '1', 'monitor:logininfor:remove', '#', 'admin', '2021-06-03 18:35:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1045, '日志导出', 501, 3, '#', '', 'F', '0', '1', 'monitor:logininfor:export', '#', 'admin', '2021-06-03 18:35:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1046, '账户解锁', 501, 4, '#', '', 'F', '0', '1', 'monitor:logininfor:unlock', '#', 'admin', '2021-06-03 18:35:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1047, '在线查询', 109, 1, '#', '', 'F', '0', '1', 'monitor:online:list', '#', 'admin', '2021-06-03 18:35:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1048, '批量强退', 109, 2, '#', '', 'F', '0', '1', 'monitor:online:batchForceLogout', '#', 'admin', '2021-06-03 18:35:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1049, '单条强退', 109, 3, '#', '', 'F', '0', '1', 'monitor:online:forceLogout', '#', 'admin', '2021-06-03 18:35:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1050, '任务查询', 110, 1, '#', '', 'F', '0', '1', 'monitor:job:list', '#', 'admin', '2021-06-03 18:35:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1051, '任务新增', 110, 2, '#', '', 'F', '0', '1', 'monitor:job:add', '#', 'admin', '2021-06-03 18:35:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1052, '任务修改', 110, 3, '#', '', 'F', '0', '1', 'monitor:job:edit', '#', 'admin', '2021-06-03 18:35:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1053, '任务删除', 110, 4, '#', '', 'F', '0', '1', 'monitor:job:remove', '#', 'admin', '2021-06-03 18:35:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1054, '状态修改', 110, 5, '#', '', 'F', '0', '1', 'monitor:job:changeStatus', '#', 'admin', '2021-06-03 18:35:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1055, '任务详细', 110, 6, '#', '', 'F', '0', '1', 'monitor:job:detail', '#', 'admin', '2021-06-03 18:35:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1056, '任务导出', 110, 7, '#', '', 'F', '0', '1', 'monitor:job:export', '#', 'admin', '2021-06-03 18:35:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1057, '生成查询', 115, 1, '#', '', 'F', '0', '1', 'tool:gen:list', '#', 'admin', '2021-06-03 18:35:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1058, '生成修改', 115, 2, '#', '', 'F', '0', '1', 'tool:gen:edit', '#', 'admin', '2021-06-03 18:35:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1059, '生成删除', 115, 3, '#', '', 'F', '0', '1', 'tool:gen:remove', '#', 'admin', '2021-06-03 18:35:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1060, '预览代码', 115, 4, '#', '', 'F', '0', '1', 'tool:gen:preview', '#', 'admin', '2021-06-03 18:35:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (1061, '生成代码', 115, 5, '#', '', 'F', '0', '1', 'tool:gen:code', '#', 'admin', '2021-06-03 18:35:09', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2000, '作业管理', 0, 5, '#', 'menuItem', 'M', '0', '1', NULL, 'fa fa-address-card-o', 'admin', '2021-06-04 21:41:01', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2001, '作业', 2000, 1, '/homework/leavehomework', '', 'C', '0', '1', 'homework:leavehomework:view', '#', 'admin', '2021-06-04 21:45:36', '', NULL, '作业菜单');
INSERT INTO `sys_menu` VALUES (2002, '作业查询', 2001, 1, '#', '', 'F', '0', '1', 'homework:leavehomework:list', '#', 'admin', '2021-06-04 21:45:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2003, '作业新增', 2001, 2, '#', '', 'F', '0', '1', 'homework:leavehomework:add', '#', 'admin', '2021-06-04 21:45:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2004, '作业修改', 2001, 3, '#', '', 'F', '0', '1', 'homework:leavehomework:edit', '#', 'admin', '2021-06-04 21:45:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2005, '作业删除', 2001, 4, '#', '', 'F', '0', '1', 'homework:leavehomework:remove', '#', 'admin', '2021-06-04 21:45:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2006, '作业导出', 2001, 5, '#', '', 'F', '0', '1', 'homework:leavehomework:export', '#', 'admin', '2021-06-04 21:45:36', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2007, '查看作业', 2000, 1, '/homework/Stuhomework', '', 'C', '0', '1', 'homework:Stuhomework:view', '#', 'admin', '2021-06-04 22:49:22', '', NULL, '查看作业菜单');
INSERT INTO `sys_menu` VALUES (2008, '查看作业查询', 2007, 1, '#', '', 'F', '0', '1', 'homework:Stuhomework:list', '#', 'admin', '2021-06-04 22:49:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2009, '查看作业新增', 2007, 2, '#', '', 'F', '0', '1', 'homework:Stuhomework:add', '#', 'admin', '2021-06-04 22:49:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2010, '查看作业修改', 2007, 3, '#', '', 'F', '0', '1', 'homework:Stuhomework:edit', '#', 'admin', '2021-06-04 22:49:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2011, '查看作业删除', 2007, 4, '#', '', 'F', '0', '1', 'homework:Stuhomework:remove', '#', 'admin', '2021-06-04 22:49:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2012, '查看作业导出', 2007, 5, '#', '', 'F', '0', '1', 'homework:Stuhomework:export', '#', 'admin', '2021-06-04 22:49:22', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2013, '作业详情', 2001, 6, '#', 'menuItem', 'F', '0', '1', 'homework:leavehomework:detail', '#', 'admin', '2021-06-06 21:05:02', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2014, '作业详情', 2007, 6, '#', 'menuItem', 'F', '0', '1', 'homework:Stuhomework:detail', '#', 'admin', '2021-06-06 21:05:45', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2015, '评分', 2007, 7, '#', 'menuItem', 'F', '0', '1', 'homework:Stuhomework:mark', '#', 'admin', '2021-06-06 21:06:33', '', NULL, '');
INSERT INTO `sys_menu` VALUES (2016, '写作业', 2001, 7, '#', 'menuItem', 'F', '0', '1', 'homework:leavehomework:dohomework', '#', 'admin', '2021-06-07 14:17:30', 'admin', '2021-06-07 18:58:11', '');
INSERT INTO `sys_menu` VALUES (2017, '作业情况', 2001, 8, '#', 'menuItem', 'F', '0', '1', 'homework:leavehomework:homeworkinfo', '#', 'admin', '2021-06-08 08:41:54', '', NULL, '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '公告内容',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '通知公告表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, '温馨提醒：2018-07-01 若依新版本发布啦', '2', '新版本内容', '0', 'admin', '2021-06-03 18:35:18', '', NULL, '管理员');
INSERT INTO `sys_notice` VALUES (2, '维护通知：2018-07-01 若依系统凌晨维护', '1', '维护内容', '0', 'admin', '2021-06-03 18:35:18', '', NULL, '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log`  (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) DEFAULT 0 COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) DEFAULT 0 COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '返回参数',
  `status` int(1) DEFAULT 0 COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime(0) DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 233 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '操作日志记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES (100, '代码生成', 6, 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/importTable', '127.0.0.1', '内网IP', '{\"tables\":[\"s_student_homework,s_homework\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-03 20:55:43');
INSERT INTO `sys_oper_log` VALUES (101, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', '研发部门', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"0\"],\"menuType\":[\"M\"],\"menuName\":[\"作业管理\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"\"],\"orderNum\":[\"5\"],\"icon\":[\"fa fa-address-card-o\"],\"visible\":[\"0\"],\"isRefresh\":[\"1\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-04 21:41:01');
INSERT INTO `sys_oper_log` VALUES (102, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"1\"],\"tableName\":[\"s_homework\"],\"tableComment\":[\"留作业表\"],\"className\":[\"SHomework\"],\"functionAuthor\":[\"BahetCoder\"],\"remark\":[\"留作业表\"],\"columns[0].columnId\":[\"1\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"主键\"],\"columns[0].javaType\":[\"Long\"],\"columns[0].javaField\":[\"id\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"2\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"教师id\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"teacherId\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"3\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"教师姓名\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"teacherName\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"LIKE\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"4\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"课程名称\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"courseName\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"select\"],\"columns[3].dictType\":[\"s_course_name\"],\"columns[4].columnId\":[\"5\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"作业标题\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"title\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"6\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"作业内容\"],\"columns[5].javaType\":[\"String\"],\"columns[5].javaField\":[\"content\"],\"columns[5].isInsert\":[\"1\"],\"col', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-04 21:41:09');
INSERT INTO `sys_oper_log` VALUES (103, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"2\"],\"tableName\":[\"s_student_homework\"],\"tableComment\":[\"提交作业表\"],\"className\":[\"SStudentHomework\"],\"functionAuthor\":[\"BahetCoder\"],\"remark\":[\"提交作业表\"],\"columns[0].columnId\":[\"15\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"主键\"],\"columns[0].javaType\":[\"Long\"],\"columns[0].javaField\":[\"id\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"16\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"学生id\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"studentId\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"17\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"学生姓名\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"studentName\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"LIKE\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"18\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"作业id\"],\"columns[3].javaType\":[\"Long\"],\"columns[3].javaField\":[\"homeworkId\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"19\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"作业标题\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"homeworkTitle\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"20\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"作业内容\"],\"columns[5].javaType\":[\"String\"],\"columns[5].javaField\":[\"homeworkContent\"],\"columns[5].isInsert\":[\"1\"],\"columns', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-04 21:43:59');
INSERT INTO `sys_oper_log` VALUES (104, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', '研发部门', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"1\"],\"tableName\":[\"s_homework\"],\"tableComment\":[\"留作业表\"],\"className\":[\"SHomework\"],\"functionAuthor\":[\"BahetCoder\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"1\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"主键\"],\"columns[0].javaType\":[\"Long\"],\"columns[0].javaField\":[\"id\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"2\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"教师id\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"teacherId\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"3\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"教师姓名\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"teacherName\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"LIKE\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"4\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"课程名称\"],\"columns[3].javaType\":[\"String\"],\"columns[3].javaField\":[\"courseName\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].isQuery\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"select\"],\"columns[3].dictType\":[\"s_course_name\"],\"columns[4].columnId\":[\"5\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"作业标题\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"title\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"6\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"作业内容\"],\"columns[5].javaType\":[\"String\"],\"columns[5].javaField\":[\"content\"],\"columns[5].isInsert\":[\"1\"],\"columns', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-04 21:44:34');
INSERT INTO `sys_oper_log` VALUES (105, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.download()', 'GET', 1, 'admin', '研发部门', '/tool/gen/download/s_homework', '127.0.0.1', '内网IP', '\"s_homework\"', NULL, 0, NULL, '2021-06-04 21:44:52');
INSERT INTO `sys_oper_log` VALUES (106, '岗位管理', 3, 'com.ruoyi.web.controller.system.SysPostController.remove()', 'POST', 1, 'admin', '研发部门', '/system/post/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"3\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-04 21:49:11');
INSERT INTO `sys_oper_log` VALUES (107, '岗位管理', 2, 'com.ruoyi.web.controller.system.SysPostController.editSave()', 'POST', 1, 'admin', '研发部门', '/system/post/edit', '127.0.0.1', '内网IP', '{\"postId\":[\"4\"],\"postName\":[\"普通管理员\"],\"postCode\":[\"user\"],\"postSort\":[\"3\"],\"status\":[\"0\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-04 21:49:25');
INSERT INTO `sys_oper_log` VALUES (108, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.download()', 'GET', 1, 'admin', 'A班', '/tool/gen/download/s_student_homework', '127.0.0.1', '内网IP', '\"s_student_homework\"', NULL, 0, NULL, '2021-06-04 22:31:30');
INSERT INTO `sys_oper_log` VALUES (109, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'admin', 'A班', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"studentId\":[\"admin\"],\"studentName\":[\"若依\"],\"homeworkId\":[\"1\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<p>6666666666666666666666666666666666666</p>\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-04 22:59:55');
INSERT INTO `sys_oper_log` VALUES (110, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', 'A班', '/tool/gen/synchDb/s_student_homework', '127.0.0.1', '内网IP', '\"s_student_homework\"', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-06 14:27:40');
INSERT INTO `sys_oper_log` VALUES (111, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', 'A班', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"2\"],\"tableName\":[\"s_student_homework\"],\"tableComment\":[\"提交作业表\"],\"className\":[\"SStudentHomework\"],\"functionAuthor\":[\"BahetCoder\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"15\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"主键\"],\"columns[0].javaType\":[\"Long\"],\"columns[0].javaField\":[\"id\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"16\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"学生id\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"studentId\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"17\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"学生姓名\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"studentName\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"LIKE\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"18\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"作业id\"],\"columns[3].javaType\":[\"Long\"],\"columns[3].javaField\":[\"homeworkId\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"19\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"作业标题\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"homeworkTitle\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"20\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"作业内容\"],\"columns[5].javaType\":[\"String\"],\"columns[5].javaField\":[\"homeworkContent\"],\"columns[5].isInsert\":[\"1\"],\"columns[5].i', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-06 14:28:24');
INSERT INTO `sys_oper_log` VALUES (112, '代码生成', 8, 'com.ruoyi.generator.controller.GenController.download()', 'GET', 1, 'admin', 'A班', '/tool/gen/download/s_student_homework', '127.0.0.1', '内网IP', '\"s_student_homework\"', NULL, 0, NULL, '2021-06-06 14:28:37');
INSERT INTO `sys_oper_log` VALUES (113, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.editSave()', 'POST', 1, 'admin', 'A班', '/tool/gen/edit', '127.0.0.1', '内网IP', '{\"tableId\":[\"2\"],\"tableName\":[\"s_student_homework\"],\"tableComment\":[\"提交作业表\"],\"className\":[\"SStudentHomework\"],\"functionAuthor\":[\"BahetCoder\"],\"remark\":[\"\"],\"columns[0].columnId\":[\"15\"],\"columns[0].sort\":[\"1\"],\"columns[0].columnComment\":[\"主键\"],\"columns[0].javaType\":[\"Long\"],\"columns[0].javaField\":[\"id\"],\"columns[0].isInsert\":[\"1\"],\"columns[0].queryType\":[\"EQ\"],\"columns[0].htmlType\":[\"input\"],\"columns[0].dictType\":[\"\"],\"columns[1].columnId\":[\"16\"],\"columns[1].sort\":[\"2\"],\"columns[1].columnComment\":[\"学生id\"],\"columns[1].javaType\":[\"String\"],\"columns[1].javaField\":[\"studentId\"],\"columns[1].isInsert\":[\"1\"],\"columns[1].isEdit\":[\"1\"],\"columns[1].isList\":[\"1\"],\"columns[1].queryType\":[\"EQ\"],\"columns[1].htmlType\":[\"input\"],\"columns[1].dictType\":[\"\"],\"columns[2].columnId\":[\"17\"],\"columns[2].sort\":[\"3\"],\"columns[2].columnComment\":[\"学生姓名\"],\"columns[2].javaType\":[\"String\"],\"columns[2].javaField\":[\"studentName\"],\"columns[2].isInsert\":[\"1\"],\"columns[2].isEdit\":[\"1\"],\"columns[2].isList\":[\"1\"],\"columns[2].isQuery\":[\"1\"],\"columns[2].queryType\":[\"LIKE\"],\"columns[2].htmlType\":[\"input\"],\"columns[2].dictType\":[\"\"],\"columns[3].columnId\":[\"18\"],\"columns[3].sort\":[\"4\"],\"columns[3].columnComment\":[\"作业id\"],\"columns[3].javaType\":[\"Long\"],\"columns[3].javaField\":[\"homeworkId\"],\"columns[3].isInsert\":[\"1\"],\"columns[3].isEdit\":[\"1\"],\"columns[3].isList\":[\"1\"],\"columns[3].queryType\":[\"EQ\"],\"columns[3].htmlType\":[\"input\"],\"columns[3].dictType\":[\"\"],\"columns[4].columnId\":[\"19\"],\"columns[4].sort\":[\"5\"],\"columns[4].columnComment\":[\"作业标题\"],\"columns[4].javaType\":[\"String\"],\"columns[4].javaField\":[\"homeworkTitle\"],\"columns[4].isInsert\":[\"1\"],\"columns[4].isEdit\":[\"1\"],\"columns[4].isList\":[\"1\"],\"columns[4].isQuery\":[\"1\"],\"columns[4].queryType\":[\"EQ\"],\"columns[4].htmlType\":[\"input\"],\"columns[4].dictType\":[\"\"],\"columns[5].columnId\":[\"20\"],\"columns[5].sort\":[\"6\"],\"columns[5].columnComment\":[\"作业内容\"],\"columns[5].javaType\":[\"String\"],\"columns[5].javaField\":[\"homeworkContent\"],\"columns[5].isInsert\":[\"1\"],\"columns[5].i', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-06 14:29:48');
INSERT INTO `sys_oper_log` VALUES (114, '代码生成', 2, 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', 1, 'admin', 'A班', '/tool/gen/synchDb/s_student_homework', '127.0.0.1', '内网IP', '\"s_student_homework\"', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-06 14:30:07');
INSERT INTO `sys_oper_log` VALUES (115, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'admin', 'A班', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"4\"],\"studentId\":[\"admin\"],\"studentName\":[\"若依\"],\"homeworkId\":[\"1\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkScore\":[\"3\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-06 15:32:59');
INSERT INTO `sys_oper_log` VALUES (116, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'admin', 'A班', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"3\"],\"studentId\":[\"2\"],\"studentName\":[\"2\"],\"homeworkId\":[\"2\"],\"homeworkTitle\":[\"2\"],\"homeworkScore\":[\"3.5\"],\"remark\":[\"5555\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-06 15:33:14');
INSERT INTO `sys_oper_log` VALUES (117, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'admin', 'A班', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"studentId\":[\"admin\"],\"studentName\":[\"若依\"],\"homeworkId\":[\"1\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<p>什么鬼作业这是，不清楚，呵呵呵呵</p>\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-06 15:53:36');
INSERT INTO `sys_oper_log` VALUES (118, '菜单管理', 3, 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'GET', 1, 'admin', 'A班', '/system/menu/remove/4', '127.0.0.1', '内网IP', '4', '{\r\n  \"msg\" : \"菜单已分配,不允许删除\",\r\n  \"code\" : 301\r\n}', 0, NULL, '2021-06-06 15:57:31');
INSERT INTO `sys_oper_log` VALUES (119, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'admin', 'A班', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"studentId\":[\"admin\"],\"studentName\":[\"若依\"],\"homeworkId\":[\"1\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<p>444444444444444444444444444444444</p>\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-06 16:01:30');
INSERT INTO `sys_oper_log` VALUES (120, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'admin', 'A班', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"createTime\":1622966611464,\"id\":9,\"params\":{}}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-06 16:03:31');
INSERT INTO `sys_oper_log` VALUES (121, '查看作业', 3, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.remove()', 'POST', 1, 'admin', 'A班', '/homework/Stuhomework/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"9\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-06 16:04:57');
INSERT INTO `sys_oper_log` VALUES (122, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'admin', 'A班', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"createTime\":1622967586579,\"id\":10,\"params\":{}}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-06 16:19:46');
INSERT INTO `sys_oper_log` VALUES (123, '查看作业', 3, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.remove()', 'POST', 1, 'admin', 'A班', '/homework/Stuhomework/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"10\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-06 16:19:55');
INSERT INTO `sys_oper_log` VALUES (124, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'admin', 'A班', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"createTime\":1622968065474,\"id\":11,\"params\":{}}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-06 16:27:45');
INSERT INTO `sys_oper_log` VALUES (125, '查看作业', 3, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.remove()', 'POST', 1, 'admin', 'A班', '/homework/Stuhomework/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"11\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-06 16:27:56');
INSERT INTO `sys_oper_log` VALUES (126, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'admin', 'A班', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"studentId\":[\"admin\"],\"studentName\":[\"若依\"],\"homeworkId\":[\"1\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"88888888888888888888888\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-06 16:52:42');
INSERT INTO `sys_oper_log` VALUES (127, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'admin', 'A班', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"studentId\":[\"admin\"],\"studentName\":[\"若依\"],\"homeworkId\":[\"1\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"作业内容作业内容作业内容作业内容这样内容\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-06 16:53:49');
INSERT INTO `sys_oper_log` VALUES (128, '作业', 1, 'com.ruoyi.web.controller.homework.SHomeworkController.addSave()', 'POST', 1, 'admin', 'A班', '/homework/leavehomework/add', '127.0.0.1', '内网IP', '{\"teacherId\":[\"admin\"],\"teacherName\":[\"若依\"],\"courseName\":[\"yw\"],\"title\":[\"背诵背诵呵呵呵\"],\"content\":[\"<p>呵呵呵呵呵呵呵额呵呵呵呵副书记海口市发动机和会计师东方航空觉得还是防控就是烦死了考核分就开始联发科见识到了开发回来开始地方了会计核算登录看发计划会计师发货单会计核算砥砺奋进合力科技第三方很快就多喝水弗兰克就很舒服大连科技恢复掉了精喹禾灵客家话老客户离开就合力科技老客户老客户了科技哈力克就好了客家话老客户离开就合力科技合力科技和客家话考了几回了开奖号离开就和考了几回老客户了科技合力科技合力科技哈力克和考了几回了客家话老客户了客家话考了几回了开奖号了开奖号考了几回老客户老客户客家话老客户客家话老客户卡了就哈哈老客户了科技回来就海联金汇考了几回了可好看了家里还了客家话考了几回了客家话了客家话了考了几回了客家话了客家话了客家话了开机和开机和！</p>\"],\"startTime\":[\"2021-06-01 18:06:42\"],\"endTime\":[\"2021-06-06 18:06:42\"],\"remark\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-06 18:56:51');
INSERT INTO `sys_oper_log` VALUES (129, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'admin', 'A班', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"studentId\":[\"admin\"],\"studentName\":[\"若依\"],\"homeworkId\":[\"2\"],\"homeworkTitle\":[\"背诵背诵呵呵呵\"],\"homeworkContent\":[\"呵呵呵呵呵呵呵额呵呵呵呵副书记海口市发动机和会计师东方航空觉得还是防控就是烦死了考核分就开始联发科见识到了开发回来开始地方了会计核算登录看发计划会计师发货单会计核算砥砺奋进合力科技第三方很快就多喝水弗兰克就很舒服大连科技恢复掉了精喹禾灵客家话老客户离开就合力科技老客户老客户了科技哈力克就好了客家话老客户离开就合力科技合力科技和客家话考了几回了开奖号离开就和考了几回老客户了科技合力科技合力科技哈力克和考了几回了客家话老客户了客家话考了几回了开奖号了开奖号考了几回老客户老客户客家话老客户客家话老客户卡了就哈哈老客户了科技回来就海联金汇考了几回了可好看了家里还了客家话考了几回了客家话了客家话了考了几回了客家话了客家话了客家话了开机和开机和！\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-06 18:57:06');
INSERT INTO `sys_oper_log` VALUES (130, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', 'A班', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"2001\"],\"menuType\":[\"F\"],\"menuName\":[\"作业详情\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"homework:leavehomework:detail\"],\"orderNum\":[\"6\"],\"icon\":[\"\"],\"visible\":[\"0\"],\"isRefresh\":[\"1\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-06 21:05:02');
INSERT INTO `sys_oper_log` VALUES (131, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', 'A班', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"2007\"],\"menuType\":[\"F\"],\"menuName\":[\"作业详情\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"homework:Stuhomework:detail\"],\"orderNum\":[\"6\"],\"icon\":[\"\"],\"visible\":[\"0\"],\"isRefresh\":[\"1\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-06 21:05:45');
INSERT INTO `sys_oper_log` VALUES (132, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', 'A班', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"2007\"],\"menuType\":[\"F\"],\"menuName\":[\"评分\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"homework:Stuhomework:mark\"],\"orderNum\":[\"7\"],\"icon\":[\"\"],\"visible\":[\"0\"],\"isRefresh\":[\"1\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-06 21:06:33');
INSERT INTO `sys_oper_log` VALUES (133, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'admin', 'A班', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"1\"],\"studentId\":[\"1\"],\"studentName\":[\"1\"],\"homeworkId\":[\"1\"],\"homeworkTitle\":[\"1\"],\"homeworkContent\":[\"<p>1111</p>\"],\"homeworkScore\":[\"0.50\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 13:53:32');
INSERT INTO `sys_oper_log` VALUES (134, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.editSave()', 'POST', 1, 'admin', 'A班', '/system/user/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"1\"],\"deptId\":[\"100\"],\"userName\":[\"若依\"],\"dept.deptName\":[\"XX学校\"],\"phonenumber\":[\"15888888888\"],\"email\":[\"ry@163.com\"],\"loginName\":[\"admin\"],\"sex\":[\"1\"],\"role\":[\"1\"],\"remark\":[\"管理员\"],\"status\":[\"0\"],\"roleIds\":[\"1\"],\"postIds\":[\"1\"]}', NULL, 1, '不允许操作超级管理员用户', '2021-06-07 14:04:35');
INSERT INTO `sys_oper_log` VALUES (135, '用户管理', 3, 'com.ruoyi.web.controller.system.SysUserController.remove()', 'POST', 1, 'admin', 'A班', '/system/user/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"2\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 14:04:51');
INSERT INTO `sys_oper_log` VALUES (136, '用户管理', 1, 'com.ruoyi.web.controller.system.SysUserController.addSave()', 'POST', 1, 'admin', 'A班', '/system/user/add', '127.0.0.1', '内网IP', '{\"deptId\":[\"101\"],\"userName\":[\"阿卜\"],\"deptName\":[\"信息学院\"],\"phonenumber\":[\"13099999999\"],\"email\":[\"abu@abu.com\"],\"loginName\":[\"abu\"],\"sex\":[\"0\"],\"role\":[\"3\"],\"remark\":[\"信息学院老师\"],\"status\":[\"0\"],\"roleIds\":[\"3\"],\"postIds\":[\"1\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 14:08:07');
INSERT INTO `sys_oper_log` VALUES (137, '用户管理', 1, 'com.ruoyi.web.controller.system.SysUserController.addSave()', 'POST', 1, 'admin', 'A班', '/system/user/add', '127.0.0.1', '内网IP', '{\"deptId\":[\"103\"],\"userName\":[\"张三\"],\"deptName\":[\"A班\"],\"phonenumber\":[\"13099999991\"],\"email\":[\"zs@abu.com\"],\"loginName\":[\"zs\"],\"sex\":[\"0\"],\"role\":[\"4\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"4\"],\"postIds\":[\"2\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 14:08:42');
INSERT INTO `sys_oper_log` VALUES (138, '用户管理', 1, 'com.ruoyi.web.controller.system.SysUserController.addSave()', 'POST', 1, 'admin', 'A班', '/system/user/add', '127.0.0.1', '内网IP', '{\"deptId\":[\"103\"],\"userName\":[\"李四\"],\"deptName\":[\"A班\"],\"phonenumber\":[\"13099999992\"],\"email\":[\"ls@abu.com\"],\"loginName\":[\"ls\"],\"sex\":[\"0\"],\"role\":[\"2\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"2\"],\"postIds\":[\"2\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 14:09:13');
INSERT INTO `sys_oper_log` VALUES (139, '用户管理', 1, 'com.ruoyi.web.controller.system.SysUserController.addSave()', 'POST', 1, 'admin', 'A班', '/system/user/add', '127.0.0.1', '内网IP', '{\"deptId\":[\"104\"],\"userName\":[\"王明\"],\"deptName\":[\"B班\"],\"phonenumber\":[\"13099999993\"],\"email\":[\"wm@abu.com\"],\"loginName\":[\"wm\"],\"sex\":[\"0\"],\"role\":[\"4\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"4\"],\"postIds\":[\"2\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 14:10:00');
INSERT INTO `sys_oper_log` VALUES (140, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.editSave()', 'POST', 1, 'admin', 'A班', '/system/user/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"102\"],\"deptId\":[\"103\"],\"userName\":[\"李四\"],\"dept.deptName\":[\"A班\"],\"phonenumber\":[\"13099999992\"],\"email\":[\"ls@abu.com\"],\"loginName\":[\"ls\"],\"sex\":[\"0\"],\"role\":[\"4\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"4\"],\"postIds\":[\"2\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 14:10:08');
INSERT INTO `sys_oper_log` VALUES (141, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.editSave()', 'POST', 1, 'admin', 'A班', '/system/user/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"101\"],\"deptId\":[\"103\"],\"userName\":[\"张三\"],\"dept.deptName\":[\"A班\"],\"phonenumber\":[\"13099999991\"],\"email\":[\"zs@abu.com\"],\"loginName\":[\"zs\"],\"sex\":[\"0\"],\"role\":[\"4\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"4\"],\"postIds\":[\"2\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 14:10:13');
INSERT INTO `sys_oper_log` VALUES (142, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.editSave()', 'POST', 1, 'admin', 'A班', '/system/user/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"102\"],\"deptId\":[\"103\"],\"userName\":[\"李四\"],\"dept.deptName\":[\"A班\"],\"phonenumber\":[\"13099999992\"],\"email\":[\"ls@abu.com\"],\"loginName\":[\"ls\"],\"sex\":[\"0\"],\"role\":[\"4\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"4\"],\"postIds\":[\"2\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 14:10:16');
INSERT INTO `sys_oper_log` VALUES (143, '用户管理', 2, 'com.ruoyi.web.controller.system.SysUserController.editSave()', 'POST', 1, 'admin', 'A班', '/system/user/edit', '127.0.0.1', '内网IP', '{\"userId\":[\"103\"],\"deptId\":[\"104\"],\"userName\":[\"王明\"],\"dept.deptName\":[\"B班\"],\"phonenumber\":[\"13099999993\"],\"email\":[\"wm@abu.com\"],\"loginName\":[\"wm\"],\"sex\":[\"0\"],\"role\":[\"4\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"4\"],\"postIds\":[\"2\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 14:10:22');
INSERT INTO `sys_oper_log` VALUES (144, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', 'XX学校', '/system/role/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"4\"],\"roleName\":[\"学生角色\"],\"roleKey\":[\"sys_student_role\"],\"roleSort\":[\"4\"],\"status\":[\"0\"],\"remark\":[\"学生角色\"],\"menuIds\":[\"2000,2001,2002,2007,2008,2014\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 14:11:33');
INSERT INTO `sys_oper_log` VALUES (145, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', 'XX学校', '/system/role/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"3\"],\"roleName\":[\"教师角色\"],\"roleKey\":[\"sys_teacher_role\"],\"roleSort\":[\"3\"],\"status\":[\"0\"],\"remark\":[\"教师角色\"],\"menuIds\":[\"2000,2001,2002,2003,2004,2005,2006,2013,2007,2008,2014,2015\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 14:12:29');
INSERT INTO `sys_oper_log` VALUES (146, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', 'XX学校', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"2001\"],\"menuType\":[\"F\"],\"menuName\":[\"写作业\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"leavehomework:leavehomework:dohomework\"],\"orderNum\":[\"7\"],\"icon\":[\"\"],\"visible\":[\"0\"],\"isRefresh\":[\"1\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 14:17:30');
INSERT INTO `sys_oper_log` VALUES (147, '作业', 2, 'com.ruoyi.web.controller.homework.SHomeworkController.editSave()', 'POST', 1, 'abu', '信息学院', '/homework/leavehomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"1\"],\"teacherId\":[\"1\"],\"teacherName\":[\"阿某\"],\"courseName\":[\"yw\"],\"title\":[\"背诵阿房宫赋\"],\"content\":[\"<p>背诵阿房宫赋背诵阿房宫赋背诵阿房宫赋背诵阿房宫赋背诵阿房宫赋<br></p>\"],\"startTime\":[\"2021-06-01 05:06:26\"],\"endTime\":[\"2021-06-03 05:06:26\"],\"remark\":[\"背诵阿房宫赋\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 14:28:00');
INSERT INTO `sys_oper_log` VALUES (148, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.authDataScopeSave()', 'POST', 1, 'admin', 'XX学校', '/system/role/authDataScope', '127.0.0.1', '内网IP', '{\"roleId\":[\"3\"],\"roleName\":[\"教师角色\"],\"roleKey\":[\"sys_teacher_role\"],\"dataScope\":[\"5\"],\"deptIds\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 14:33:43');
INSERT INTO `sys_oper_log` VALUES (149, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.authDataScopeSave()', 'POST', 1, 'admin', 'XX学校', '/system/role/authDataScope', '127.0.0.1', '内网IP', '{\"roleId\":[\"4\"],\"roleName\":[\"学生角色\"],\"roleKey\":[\"sys_student_role\"],\"dataScope\":[\"5\"],\"deptIds\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 14:33:51');
INSERT INTO `sys_oper_log` VALUES (150, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', 'XX学校', '/system/role/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"4\"],\"roleName\":[\"学生角色\"],\"roleKey\":[\"sys_student_role\"],\"roleSort\":[\"4\"],\"status\":[\"0\"],\"remark\":[\"学生角色\"],\"menuIds\":[\"2000,2001,2002,2013,2016,2007,2008,2014\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 16:58:31');
INSERT INTO `sys_oper_log` VALUES (151, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', 'XX学校', '/system/role/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"3\"],\"roleName\":[\"教师角色\"],\"roleKey\":[\"sys_teacher_role\"],\"roleSort\":[\"3\"],\"status\":[\"0\"],\"remark\":[\"教师角色\"],\"menuIds\":[\"2000,2001,2002,2003,2004,2005,2006,2013,2007,2008,2014,2015\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 16:58:43');
INSERT INTO `sys_oper_log` VALUES (152, '菜单管理', 2, 'com.ruoyi.web.controller.system.SysMenuController.editSave()', 'POST', 1, 'admin', 'XX学校', '/system/menu/edit', '127.0.0.1', '内网IP', '{\"menuId\":[\"2016\"],\"parentId\":[\"2001\"],\"menuType\":[\"F\"],\"menuName\":[\"写作业\"],\"url\":[\"#\"],\"target\":[\"menuItem\"],\"perms\":[\"homework:leavehomework:dohomework\"],\"orderNum\":[\"7\"],\"icon\":[\"#\"],\"visible\":[\"0\"],\"isRefresh\":[\"1\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 18:58:12');
INSERT INTO `sys_oper_log` VALUES (153, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', 'XX学校', '/system/role/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"4\"],\"roleName\":[\"学生角色\"],\"roleKey\":[\"sys_student_role\"],\"roleSort\":[\"4\"],\"status\":[\"0\"],\"remark\":[\"学生角色\"],\"menuIds\":[\"2000,2001,2002,2013,2016,2007,2008,2010,2014\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 20:40:05');
INSERT INTO `sys_oper_log` VALUES (154, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'admin', 'XX学校', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"studentId\":[\"1\"],\"studentName\":[\"若依\"],\"homeworkId\":[\"1\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"999\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 21:28:36');
INSERT INTO `sys_oper_log` VALUES (155, '作业', 1, 'com.ruoyi.web.controller.homework.SHomeworkController.addSave()', 'POST', 1, 'admin', 'XX学校', '/homework/leavehomework/add', '127.0.0.1', '内网IP', '{\"teacherId\":[\"1\"],\"teacherName\":[\"若依\"],\"courseName\":[\"yw\"],\"title\":[\"123\"],\"content\":[\"<p>123</p>\"],\"startTime\":[\"2021-06-01 21:06:01\"],\"endTime\":[\"2021-06-07 21:06:01\"],\"remark\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 21:29:21');
INSERT INTO `sys_oper_log` VALUES (156, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'admin', 'XX学校', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"studentId\":[\"1\"],\"studentName\":[\"若依\"],\"homeworkId\":[\"1\"],\"homeworkTitle\":[\"123\"],\"homeworkContent\":[\"11111\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 21:29:29');
INSERT INTO `sys_oper_log` VALUES (157, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'admin', 'XX学校', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"studentId\":[\"1\"],\"studentName\":[\"若依\"],\"homeworkId\":[\"1\"],\"homeworkTitle\":[\"123\"],\"homeworkContent\":[\"99999999\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 21:29:52');
INSERT INTO `sys_oper_log` VALUES (158, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'admin', 'XX学校', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"studentId\":[\"1\"],\"studentName\":[\"若依\"],\"homeworkId\":[\"1\"],\"homeworkTitle\":[\"123\"],\"homeworkContent\":[\"22222222\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 21:31:26');
INSERT INTO `sys_oper_log` VALUES (159, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'admin', 'XX学校', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"studentId\":[\"1\"],\"studentName\":[\"若依\"],\"homeworkId\":[\"1\"],\"homeworkTitle\":[\"123\"],\"homeworkContent\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 21:35:53');
INSERT INTO `sys_oper_log` VALUES (160, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'admin', 'XX学校', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"studentId\":[\"1\"],\"studentName\":[\"若依\"],\"homeworkId\":[\"1\"],\"homeworkTitle\":[\"123\"],\"homeworkContent\":[\"000\"]}', NULL, 1, '作业提交时间已截止！', '2021-06-07 21:39:14');
INSERT INTO `sys_oper_log` VALUES (161, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'admin', 'XX学校', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"studentId\":[\"1\"],\"studentName\":[\"若依\"],\"homeworkId\":[\"1\"],\"homeworkTitle\":[\"123\"],\"homeworkContent\":[\"99\"]}', NULL, 1, '作业提交时间已截止！', '2021-06-07 21:39:58');
INSERT INTO `sys_oper_log` VALUES (162, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'admin', 'XX学校', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"studentId\":[\"1\"],\"studentName\":[\"若依\"],\"homeworkId\":[\"1\"],\"homeworkTitle\":[\"123\"],\"homeworkContent\":[\"99\"]}', NULL, 1, '作业提交时间已截止！', '2021-06-07 21:40:03');
INSERT INTO `sys_oper_log` VALUES (163, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'admin', 'XX学校', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"studentId\":[\"1\"],\"studentName\":[\"若依\"],\"homeworkId\":[\"1\"],\"homeworkTitle\":[\"123\"],\"homeworkContent\":[\"99\"]}', NULL, 1, '作业提交时间已截止！', '2021-06-07 21:40:09');
INSERT INTO `sys_oper_log` VALUES (164, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'admin', 'XX学校', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"studentId\":[\"1\"],\"studentName\":[\"若依\"],\"homeworkId\":[\"1\"],\"homeworkTitle\":[\"123\"],\"homeworkContent\":[\"99\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 21:40:43');
INSERT INTO `sys_oper_log` VALUES (165, '查看作业', 3, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.remove()', 'POST', 1, 'admin', 'XX学校', '/homework/Stuhomework/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"1\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 21:41:15');
INSERT INTO `sys_oper_log` VALUES (166, '查看作业', 3, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.remove()', 'POST', 1, 'admin', 'XX学校', '/homework/Stuhomework/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"2\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 21:41:17');
INSERT INTO `sys_oper_log` VALUES (167, '查看作业', 3, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.remove()', 'POST', 1, 'admin', 'XX学校', '/homework/Stuhomework/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"3\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 21:41:18');
INSERT INTO `sys_oper_log` VALUES (168, '查看作业', 3, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.remove()', 'POST', 1, 'admin', 'XX学校', '/homework/Stuhomework/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"4\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 21:41:20');
INSERT INTO `sys_oper_log` VALUES (169, '查看作业', 3, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.remove()', 'POST', 1, 'admin', 'XX学校', '/homework/Stuhomework/remove', '127.0.0.1', '内网IP', '{\"ids\":[\"5\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 21:41:22');
INSERT INTO `sys_oper_log` VALUES (170, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'admin', 'XX学校', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"studentId\":[\"1\"],\"studentName\":[\"若依\"],\"homeworkId\":[\"1\"],\"homeworkTitle\":[\"123\"],\"homeworkContent\":[\"666666666666666\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 21:41:28');
INSERT INTO `sys_oper_log` VALUES (171, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'admin', 'XX学校', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"6\"],\"studentId\":[\"1\"],\"studentName\":[\"若依\"],\"homeworkId\":[\"1\"],\"homeworkTitle\":[\"123\"],\"homeworkScore\":[\"100\"],\"remark\":[\"100\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-07 21:41:42');
INSERT INTO `sys_oper_log` VALUES (172, '菜单管理', 1, 'com.ruoyi.web.controller.system.SysMenuController.addSave()', 'POST', 1, 'admin', 'XX学校', '/system/menu/add', '127.0.0.1', '内网IP', '{\"parentId\":[\"2001\"],\"menuType\":[\"F\"],\"menuName\":[\"作业情况\"],\"url\":[\"\"],\"target\":[\"menuItem\"],\"perms\":[\"homework:leavehomework:homeworkinfo\"],\"orderNum\":[\"8\"],\"icon\":[\"\"],\"visible\":[\"0\"],\"isRefresh\":[\"1\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 08:41:54');
INSERT INTO `sys_oper_log` VALUES (173, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', 'XX学校', '/system/role/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"3\"],\"roleName\":[\"教师角色\"],\"roleKey\":[\"sys_teacher_role\"],\"roleSort\":[\"3\"],\"status\":[\"0\"],\"remark\":[\"教师角色\"],\"menuIds\":[\"2000,2001,2002,2003,2004,2005,2006,2013,2017,2007,2008,2014,2015\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 08:42:33');
INSERT INTO `sys_oper_log` VALUES (174, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', 'XX学校', '/system/role/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"4\"],\"roleName\":[\"学生角色\"],\"roleKey\":[\"sys_student_role\"],\"roleSort\":[\"4\"],\"status\":[\"0\"],\"remark\":[\"学生角色\"],\"menuIds\":[\"2000,2001,2002,2013,2016,2007,2008,2009,2010,2014\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 09:31:38');
INSERT INTO `sys_oper_log` VALUES (175, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"studentId\":[\"101\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"1\"],\"homeworkTitle\":[\"123\"],\"homeworkContent\":[\"3546516516516515\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 09:31:46');
INSERT INTO `sys_oper_log` VALUES (176, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"studentId\":[\"101\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"1\"],\"homeworkTitle\":[\"123\"],\"homeworkContent\":[\"jughhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhjhgjhgjkhgjkkfgfjkhgjkhgkj\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 09:41:05');
INSERT INTO `sys_oper_log` VALUES (177, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"studentId\":[\"101\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"1\"],\"homeworkTitle\":[\"123\"],\"homeworkContent\":[\"65465465456412121235451231251发多少是是是是是是所所所所所所所所所所所所所所所所所所所所所所所所所所所所所所所所所所所所所所所所所所所所所所所所所所所所所所所\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 09:49:35');
INSERT INTO `sys_oper_log` VALUES (178, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"studentId\":[\"101\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"1\"],\"homeworkTitle\":[\"123\"],\"homeworkContent\":[\"你好呵呵呵或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或或红红火火恍恍惚惚或或或或或或或或或或或或或或或或或或或或或或或或或或红红火火恍恍惚惚或或或或或或或或或或或或或或或或或红红火火恍恍惚惚或或或或或或或或或或或或或或红红火火恍恍惚惚或或或或或或或或或或或或或或或或或或或或红红火火恍恍惚惚或或或或或或或或或或或或或或或或或或或或或！！！！！！！！\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 09:52:52');
INSERT INTO `sys_oper_log` VALUES (179, '作业', 1, 'com.ruoyi.web.controller.homework.SHomeworkController.addSave()', 'POST', 1, 'admin', 'XX学校', '/homework/leavehomework/add', '127.0.0.1', '内网IP', '{\"teacherId\":[\"1\"],\"teacherName\":[\"若依\"],\"deptName\":[\"103\"],\"courseName\":[\"yw\"],\"title\":[\"轮播图1\"],\"content\":[\"<p>6666666666</p>\"],\"startTime\":[\"2021-06-08 10:06:39\"],\"endTime\":[\"2021-06-09 14:06:39\"],\"remark\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 10:27:04');
INSERT INTO `sys_oper_log` VALUES (180, '作业', 1, 'com.ruoyi.web.controller.homework.SHomeworkController.addSave()', 'POST', 1, 'admin', 'XX学校', '/homework/leavehomework/add', '127.0.0.1', '内网IP', '{\"teacherId\":[\"1\"],\"teacherName\":[\"若依\"],\"deptName\":[\"103\"],\"courseName\":[\"yw\"],\"title\":[\"轮播图2\"],\"content\":[\"<p>666666666666666666666666666666666666666666666</p>\"],\"startTime\":[\"2021-06-08 10:06:55\"],\"endTime\":[\"2021-06-09 10:06:55\"],\"remark\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 10:58:21');
INSERT INTO `sys_oper_log` VALUES (181, '作业', 1, 'com.ruoyi.web.controller.homework.SHomeworkController.addSave()', 'POST', 1, 'admin', 'XX学校', '/homework/leavehomework/add', '127.0.0.1', '内网IP', '{\"teacherId\":[\"1\"],\"teacherName\":[\"若依\"],\"stuclassid\":[\"103\"],\"stuclassname\":[\"\"],\"courseName\":[\"yw\"],\"title\":[\" 暂不支持支付\"],\"content\":[\"<p>&nbsp;暂不支持支付&nbsp;暂不支持支付&nbsp;暂不支持支付&nbsp;暂不支持支付&nbsp;暂不支持支付&nbsp;暂不支持支付<br></p>\"],\"startTime\":[\"2021-06-08 08:06:20\"],\"endTime\":[\"2021-06-09 08:06:20\"],\"remark\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 11:01:45');
INSERT INTO `sys_oper_log` VALUES (182, '作业', 1, 'com.ruoyi.web.controller.homework.SHomeworkController.addSave()', 'POST', 1, 'admin', 'XX学校', '/homework/leavehomework/add', '127.0.0.1', '内网IP', '{\"teacherId\":[\"1\"],\"teacherName\":[\"若依\"],\"stuclassid\":[\"104\"],\"stuclassname\":[\"B班\"],\"courseName\":[\"yw\"],\"title\":[\"测试作业\"],\"content\":[\"<p>测试作业测试作业测试作业测试作业测试作业测试作业测试作业测试作业测试作业测试作业测试作业测试作业<br></p>\"],\"startTime\":[\"2021-06-08 09:06:27\"],\"endTime\":[\"2021-06-09 18:06:27\"],\"remark\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 11:03:09');
INSERT INTO `sys_oper_log` VALUES (183, '用户管理', 1, 'com.ruoyi.web.controller.system.SysUserController.addSave()', 'POST', 1, 'admin', 'XX学校', '/system/user/add', '127.0.0.1', '内网IP', '{\"deptId\":[\"101\"],\"userName\":[\"张翰\"],\"deptName\":[\"信息学院\"],\"phonenumber\":[\"13099999988\"],\"email\":[\"zh@abu.com\"],\"loginName\":[\"zh\"],\"sex\":[\"0\"],\"role\":[\"3\"],\"remark\":[\"张翰老师\"],\"status\":[\"0\"],\"roleIds\":[\"3\"],\"postIds\":[\"1\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 12:26:21');
INSERT INTO `sys_oper_log` VALUES (184, '作业', 1, 'com.ruoyi.web.controller.homework.SHomeworkController.addSave()', 'POST', 1, 'zh', '信息学院', '/homework/leavehomework/add', '127.0.0.1', '内网IP', '{\"teacherId\":[\"104\"],\"teacherName\":[\"张翰\"],\"stuclassid\":[\"\"],\"stuclassname\":[\"\"],\"courseName\":[\"yw\"],\"title\":[\"TTTTTTTTTT\"],\"content\":[\"<p>RRRRRRRRRRRRRRRRRRRRRR</p>\"],\"startTime\":[\"2021-06-08 12:06:30\"],\"endTime\":[\"2021-06-10 12:06:30\"],\"remark\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 12:49:49');
INSERT INTO `sys_oper_log` VALUES (185, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', 'XX学校', '/system/role/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"3\"],\"roleName\":[\"教师角色\"],\"roleKey\":[\"sys_teacher_role\"],\"roleSort\":[\"3\"],\"status\":[\"0\"],\"remark\":[\"教师角色\"],\"menuIds\":[\"1,103,1016,2000,2001,2002,2003,2004,2005,2006,2013,2017,2007,2008,2014,2015\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 12:56:37');
INSERT INTO `sys_oper_log` VALUES (186, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.authDataScopeSave()', 'POST', 1, 'admin', 'XX学校', '/system/role/authDataScope', '127.0.0.1', '内网IP', '{\"roleId\":[\"3\"],\"roleName\":[\"教师角色\"],\"roleKey\":[\"sys_teacher_role\"],\"dataScope\":[\"4\"],\"deptIds\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 12:58:32');
INSERT INTO `sys_oper_log` VALUES (187, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.authDataScopeSave()', 'POST', 1, 'admin', 'XX学校', '/system/role/authDataScope', '127.0.0.1', '内网IP', '{\"roleId\":[\"3\"],\"roleName\":[\"教师角色\"],\"roleKey\":[\"sys_teacher_role\"],\"dataScope\":[\"3\"],\"deptIds\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 12:59:03');
INSERT INTO `sys_oper_log` VALUES (188, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.authDataScopeSave()', 'POST', 1, 'admin', 'XX学校', '/system/role/authDataScope', '127.0.0.1', '内网IP', '{\"roleId\":[\"3\"],\"roleName\":[\"教师角色\"],\"roleKey\":[\"sys_teacher_role\"],\"dataScope\":[\"2\"],\"deptIds\":[\"100,101,103,104,105,106,107\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 13:00:04');
INSERT INTO `sys_oper_log` VALUES (189, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.authDataScopeSave()', 'POST', 1, 'admin', 'XX学校', '/system/role/authDataScope', '127.0.0.1', '内网IP', '{\"roleId\":[\"3\"],\"roleName\":[\"教师角色\"],\"roleKey\":[\"sys_teacher_role\"],\"dataScope\":[\"2\"],\"deptIds\":[\"100,101,103,104,105,106,107\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 13:01:45');
INSERT INTO `sys_oper_log` VALUES (190, '作业', 1, 'com.ruoyi.web.controller.homework.SHomeworkController.addSave()', 'POST', 1, 'abu', '信息学院', '/homework/leavehomework/add', '127.0.0.1', '内网IP', '{\"teacherId\":[\"100\"],\"teacherName\":[\"阿卜\"],\"stuclassname\":[\"A班\"],\"stuclassid\":[\"103\"],\"courseName\":[\"sx\"],\"title\":[\"数学作业呀呀呀呀\"],\"content\":[\"<p>数学作业呀呀呀呀数学作业呀呀呀呀数学作业呀呀呀呀数学作业呀呀呀呀数学作业呀呀呀呀数学作业呀呀呀呀数学作业呀呀呀呀数学作业呀呀呀呀数学作业呀呀呀呀数学作业呀呀呀呀<br></p>\"],\"startTime\":[\"2021-06-08 13:06:23\"],\"endTime\":[\"2021-06-09 13:06:23\"],\"remark\":[\"\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 13:03:56');
INSERT INTO `sys_oper_log` VALUES (191, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'wm', 'B班', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"studentId\":[\"103\"],\"studentName\":[\"王明\"],\"homeworkId\":[\"3\"],\"homeworkTitle\":[\"轮播图2\"],\"remark\":[\"\"],\"homeworkContent\":[\"什么破作业，不做，呵呵呵什么破作业，不做，呵呵呵什么破作业，不做，呵呵呵什么破作业，不做，呵呵呵\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 13:07:06');
INSERT INTO `sys_oper_log` VALUES (192, '用户管理', 1, 'com.ruoyi.web.controller.system.SysUserController.addSave()', 'POST', 1, 'admin', 'A班', '/system/user/add', '127.0.0.1', '内网IP', '{\"deptId\":[\"101\"],\"userName\":[\"阿卜\"],\"deptName\":[\"信息学院\"],\"phonenumber\":[\"13099999999\"],\"email\":[\"abu@abu.com\"],\"loginName\":[\"abu\"],\"sex\":[\"0\"],\"role\":[\"3\"],\"remark\":[\"阿卜老师\"],\"status\":[\"0\"],\"roleIds\":[\"3\"],\"postIds\":[\"1\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 13:31:28');
INSERT INTO `sys_oper_log` VALUES (193, '用户管理', 1, 'com.ruoyi.web.controller.system.SysUserController.addSave()', 'POST', 1, 'admin', 'A班', '/system/user/add', '127.0.0.1', '内网IP', '{\"deptId\":[\"103\"],\"userName\":[\"张三\"],\"deptName\":[\"A班\"],\"phonenumber\":[\"13099999991\"],\"email\":[\"zs@abu.com\"],\"loginName\":[\"zs\"],\"sex\":[\"0\"],\"role\":[\"4\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"4\"],\"postIds\":[\"2\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 13:31:55');
INSERT INTO `sys_oper_log` VALUES (194, '用户管理', 1, 'com.ruoyi.web.controller.system.SysUserController.addSave()', 'POST', 1, 'admin', 'A班', '/system/user/add', '127.0.0.1', '内网IP', '{\"deptId\":[\"104\"],\"userName\":[\"李四\"],\"deptName\":[\"B班\"],\"phonenumber\":[\"13099999992\"],\"email\":[\"ls@abu.com\"],\"loginName\":[\"ls\"],\"sex\":[\"0\"],\"role\":[\"4\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"4\"],\"postIds\":[\"2\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 13:32:16');
INSERT INTO `sys_oper_log` VALUES (195, '用户管理', 1, 'com.ruoyi.web.controller.system.SysUserController.addSave()', 'POST', 1, 'admin', 'A班', '/system/user/add', '127.0.0.1', '内网IP', '{\"deptId\":[\"105\"],\"userName\":[\"王明\"],\"deptName\":[\"C班\"],\"phonenumber\":[\"13099999993\"],\"email\":[\"wm@abu.com\"],\"loginName\":[\"wm\"],\"sex\":[\"0\"],\"role\":[\"4\"],\"remark\":[\"\"],\"status\":[\"0\"],\"roleIds\":[\"4\"],\"postIds\":[\"2\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 13:32:40');
INSERT INTO `sys_oper_log` VALUES (196, '作业', 1, 'com.ruoyi.web.controller.homework.SHomeworkController.addSave()', 'POST', 1, 'abu', '信息学院', '/homework/leavehomework/add', '127.0.0.1', '内网IP', '{\"teacherId\":[\"105\"],\"teacherName\":[\"阿卜\"],\"stuclassname\":[\"A班\"],\"stuclassid\":[\"103\"],\"courseName\":[\"yw\"],\"title\":[\"背诵阿房宫赋\"],\"content\":[\"<p>背诵阿房宫赋背诵阿房宫赋<br></p>\"],\"startTime\":[\"2021-06-08 10:06:33\"],\"endTime\":[\"2021-06-09 18:06:34\"],\"remark\":[\"背诵阿房宫赋\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 13:34:16');
INSERT INTO `sys_oper_log` VALUES (197, '作业', 1, 'com.ruoyi.web.controller.homework.SHomeworkController.addSave()', 'POST', 1, 'zh', '信息学院', '/homework/leavehomework/add', '127.0.0.1', '内网IP', '{\"teacherId\":[\"104\"],\"teacherName\":[\"张翰\"],\"stuclassname\":[\"B班\"],\"stuclassid\":[\"104\"],\"courseName\":[\"sx\"],\"title\":[\"把期末考试模拟卷子写完\"],\"content\":[\"<p>把期末考试模拟卷子写完<br></p>\"],\"startTime\":[\"2021-06-08 13:06:44\"],\"endTime\":[\"2021-06-09 18:06:44\"],\"remark\":[\"把期末考试模拟卷子写完\"],\"status\":[\"0\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 13:35:58');
INSERT INTO `sys_oper_log` VALUES (198, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"remark\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<b>背诵阿房宫赋已完成背诵，呵呵呵呵</b>\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 13:36:58');
INSERT INTO `sys_oper_log` VALUES (199, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'ls', 'B班', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"studentId\":[\"107\"],\"studentName\":[\"李四\"],\"homeworkId\":[\"9\"],\"homeworkTitle\":[\"把期末考试模拟卷子写完\"],\"remark\":[\"把期末考试模拟卷子写完\"],\"homeworkContent\":[\"<p>不会做呀，也不叫我写写，你是干什么吃的老师，呵呵呵</p><p><br></p>\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 13:38:17');
INSERT INTO `sys_oper_log` VALUES (200, '角色管理', 2, 'com.ruoyi.web.controller.system.SysRoleController.editSave()', 'POST', 1, 'admin', 'XX学校', '/system/role/edit', '127.0.0.1', '内网IP', '{\"roleId\":[\"3\"],\"roleName\":[\"教师角色\"],\"roleKey\":[\"sys_teacher_role\"],\"roleSort\":[\"3\"],\"status\":[\"0\"],\"remark\":[\"教师角色\"],\"menuIds\":[\"1,103,1016,2000,2001,2002,2003,2004,2005,2006,2013,2017,2007,2008,2010,2014,2015\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 13:41:29');
INSERT INTO `sys_oper_log` VALUES (201, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'zh', '信息学院', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"15\"],\"studentId\":[\"107\"],\"studentName\":[\"李四\"],\"homeworkId\":[\"9\"],\"homeworkTitle\":[\"把期末考试模拟卷子写完\"],\"homeworkScore\":[\"0\"],\"remark\":[\"给零分，什么鬼你这交的！呵呵呵\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 13:42:02');
INSERT INTO `sys_oper_log` VALUES (202, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'abu', '信息学院', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"14\"],\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkScore\":[\"100\"],\"remark\":[\"100分，背诵的不错呀！\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 13:44:22');
INSERT INTO `sys_oper_log` VALUES (203, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'abu', '信息学院', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"14\"],\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkScore\":[\"100.00\"],\"remark\":[\"100分，背诵的不错呀！\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 14:10:49');
INSERT INTO `sys_oper_log` VALUES (204, '字典类型', 1, 'com.ruoyi.web.controller.system.SysDictTypeController.addSave()', 'POST', 1, 'admin', 'XX学校', '/system/dict/add', '127.0.0.1', '内网IP', '{\"dictName\":[\"作业完成状态\"],\"dictType\":[\"dohomework_statues\"],\"status\":[\"0\"],\"remark\":[\"学生作业完成状态\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 14:13:02');
INSERT INTO `sys_oper_log` VALUES (205, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', 'XX学校', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\"dictLabel\":[\"已完成\"],\"dictValue\":[\"0\"],\"dictType\":[\"dohomework_statues\"],\"cssClass\":[\"\"],\"dictSort\":[\"1\"],\"listClass\":[\"success\"],\"isDefault\":[\"Y\"],\"status\":[\"0\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 14:13:41');
INSERT INTO `sys_oper_log` VALUES (206, '字典数据', 1, 'com.ruoyi.web.controller.system.SysDictDataController.addSave()', 'POST', 1, 'admin', 'XX学校', '/system/dict/data/add', '127.0.0.1', '内网IP', '{\"dictLabel\":[\"已批改\"],\"dictValue\":[\"1\"],\"dictType\":[\"dohomework_statues\"],\"cssClass\":[\"\"],\"dictSort\":[\"2\"],\"listClass\":[\"primary\"],\"isDefault\":[\"Y\"],\"status\":[\"0\"],\"remark\":[\"\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 14:14:07');
INSERT INTO `sys_oper_log` VALUES (207, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"14\"],\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<b>背诵阿房宫赋已完成背诵，呵呵呵呵</b>\"],\"homeworkScore\":[\"100.00\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 14:17:49');
INSERT INTO `sys_oper_log` VALUES (208, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"14\"],\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<b>背诵阿房宫赋已完成背诵，呵呵呵呵</b>\"],\"homeworkScore\":[\"100.00\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 14:20:24');
INSERT INTO `sys_oper_log` VALUES (209, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"14\"],\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<b>背诵阿房宫赋已完成背诵，呵呵呵呵</b>\"],\"homeworkScore\":[\"100.00\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 14:25:15');
INSERT INTO `sys_oper_log` VALUES (210, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"14\"],\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<b>背诵阿房宫赋已完成背诵，呵呵呵呵</b>\"],\"homeworkScore\":[\"100.00\"]}', NULL, 1, '', '2021-06-08 14:28:32');
INSERT INTO `sys_oper_log` VALUES (211, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"14\"],\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<b>背诵阿房宫赋已完成背诵，呵呵呵呵</b>\"],\"homeworkScore\":[\"100.00\"]}', NULL, 1, '', '2021-06-08 14:29:16');
INSERT INTO `sys_oper_log` VALUES (212, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"14\"],\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<b>背诵阿房宫赋已完成背诵，呵呵呵呵</b>\"],\"homeworkScore\":[\"100.00\"]}', NULL, 1, '', '2021-06-08 14:34:29');
INSERT INTO `sys_oper_log` VALUES (213, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"14\"],\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<b>背诵阿房宫赋已完成背诵，呵呵呵呵</b>\"],\"homeworkScore\":[\"100.00\"]}', NULL, 1, '', '2021-06-08 14:34:57');
INSERT INTO `sys_oper_log` VALUES (214, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"14\"],\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<b>背诵阿房宫赋已完成背诵，呵呵呵呵</b>\"],\"homeworkScore\":[\"100.00\"]}', NULL, 1, '', '2021-06-08 14:43:35');
INSERT INTO `sys_oper_log` VALUES (215, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"14\"],\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<b>背诵阿房宫赋已完成背诵，呵呵呵呵</b>\"],\"homeworkScore\":[\"100.00\"]}', NULL, 1, '', '2021-06-08 14:46:28');
INSERT INTO `sys_oper_log` VALUES (216, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"14\"],\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<b>背诵阿房宫赋已完成背诵，呵呵呵呵</b>\"],\"homeworkScore\":[\"100.00\"]}', NULL, 1, '', '2021-06-08 14:51:16');
INSERT INTO `sys_oper_log` VALUES (217, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"14\"],\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<b>背诵阿房宫赋已完成背诵，呵呵呵呵</b>\"],\"homeworkScore\":[\"100.00\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 14:54:02');
INSERT INTO `sys_oper_log` VALUES (218, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"14\"],\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<b>背诵阿房宫赋已完成背诵，呵呵呵呵</b>\"],\"homeworkScore\":[\"100.00\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 15:00:34');
INSERT INTO `sys_oper_log` VALUES (219, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"14\"],\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<b>背诵阿房宫赋已完成背诵，呵呵呵呵</b>\"],\"homeworkScore\":[\"100.00\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 15:06:38');
INSERT INTO `sys_oper_log` VALUES (220, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"14\"],\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<b>背诵阿房宫赋已完成背诵，呵呵呵呵</b>\"],\"homeworkScore\":[\"100.00\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 15:25:43');
INSERT INTO `sys_oper_log` VALUES (221, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"14\"],\"state\":[\"1\"],\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<b>背诵阿房宫赋已完成背诵，呵呵呵呵</b>\"],\"homeworkScore\":[\"100.00\"]}', NULL, 1, '作业已批改，无法再修改！', '2021-06-08 15:32:40');
INSERT INTO `sys_oper_log` VALUES (222, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"14\"],\"state\":[\"1\"],\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<b>背诵阿房宫赋已完成背诵，呵呵呵呵</b>\"],\"homeworkScore\":[\"100.00\"]}', NULL, 1, '作业已批改，无法再修改！', '2021-06-08 15:32:49');
INSERT INTO `sys_oper_log` VALUES (223, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"14\"],\"state\":[\"1\"],\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<b>背诵阿房宫赋已完成背诵，呵呵呵呵</b>\"],\"homeworkScore\":[\"100.00\"]}', NULL, 1, '作业已批改，无法再修改！', '2021-06-08 15:36:20');
INSERT INTO `sys_oper_log` VALUES (224, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"14\"],\"state\":[\"1\"],\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<b>背诵阿房宫赋已完成背诵，呵呵呵呵</b>\"],\"homeworkScore\":[\"100.00\"]}', NULL, 1, '作业已批改，无法再修改！', '2021-06-08 15:46:57');
INSERT INTO `sys_oper_log` VALUES (225, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"14\"],\"state\":[\"1\"],\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<b>背诵阿房宫赋已完成背诵，呵呵呵呵</b>\"],\"homeworkScore\":[\"100.00\"]}', NULL, 1, '作业已批改，无法再修改！', '2021-06-08 15:49:16');
INSERT INTO `sys_oper_log` VALUES (226, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"14\"],\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<b>背诵阿房宫赋已完成背诵，呵呵呵呵</b>\"],\"homeworkScore\":[\"100.00\"]}', NULL, 1, '作业已批改，无法再修改！', '2021-06-08 15:50:42');
INSERT INTO `sys_oper_log` VALUES (227, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"14\"],\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<b>背诵阿房宫赋已完成背诵，呵呵呵呵</b>\"],\"homeworkScore\":[\"100.00\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 15:52:44');
INSERT INTO `sys_oper_log` VALUES (228, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"remark\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"55555\"]}', NULL, 1, '您已提交作业，不允许重复提交！', '2021-06-08 16:00:35');
INSERT INTO `sys_oper_log` VALUES (229, '查看作业', 1, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.addSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/add', '127.0.0.1', '内网IP', '{\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"remark\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<span style=\\\"font-weight: 700; color: rgb(103, 106, 108);\\\">背诵阿房宫赋已完成背诵，呵呵呵呵</span>\"]}', NULL, 1, '您已提交作业，不允许重复提交！', '2021-06-08 16:11:47');
INSERT INTO `sys_oper_log` VALUES (230, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'abu', '信息学院', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"14\"],\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkScore\":[\"100.00\"],\"remark\":[\"100分，背诵的不错呀！\"]}', '{\r\n  \"msg\" : \"操作成功\",\r\n  \"code\" : 0\r\n}', 0, NULL, '2021-06-08 16:12:15');
INSERT INTO `sys_oper_log` VALUES (231, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"14\"],\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<b>背诵阿房宫赋已完成背诵，呵呵呵呵</b>\"],\"homeworkScore\":[\"100.00\"]}', NULL, 1, '作业已批改，无法再修改！', '2021-06-08 16:12:28');
INSERT INTO `sys_oper_log` VALUES (232, '查看作业', 2, 'com.ruoyi.web.controller.homework.SStudentHomeworkController.editSave()', 'POST', 1, 'zs', 'A班', '/homework/Stuhomework/edit', '127.0.0.1', '内网IP', '{\"id\":[\"14\"],\"studentId\":[\"106\"],\"studentName\":[\"张三\"],\"homeworkId\":[\"8\"],\"homeworkTitle\":[\"背诵阿房宫赋\"],\"homeworkContent\":[\"<b>背诵阿房宫赋已完成背诵，呵呵呵呵</b>\"],\"homeworkScore\":[\"100.00\"]}', NULL, 1, '作业已批改，无法再修改！', '2021-06-08 16:12:37');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post`  (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '岗位信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES (1, 'teacher', '教师', 1, '0', 'admin', '2021-05-31 17:04:19', 'admin', '2021-06-01 09:49:00', '');
INSERT INTO `sys_post` VALUES (2, 'student', '学生', 2, '0', 'admin', '2021-05-31 17:04:19', 'admin', '2021-06-01 09:49:28', '');
INSERT INTO `sys_post` VALUES (3, 'user', '普通管理员', 3, '0', 'admin', '2021-06-03 18:35:06', 'admin', '2021-06-04 21:49:25', '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 100 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'admin', 1, '1', '0', '0', 'admin', '2021-05-31 17:04:19', '', NULL, '超级管理员');
INSERT INTO `sys_role` VALUES (2, '普通角色', 'common', 2, '2', '0', '0', 'admin', '2021-05-31 17:04:19', '', NULL, '普通角色');
INSERT INTO `sys_role` VALUES (3, '教师角色', 'sys_teacher_role', 3, '2', '0', '0', 'admin', '2021-06-01 10:15:31', 'admin', '2021-06-08 13:41:29', '教师角色');
INSERT INTO `sys_role` VALUES (4, '学生角色', 'sys_student_role', 4, '5', '0', '0', 'admin', '2021-06-01 10:16:06', 'admin', '2021-06-08 09:31:38', '学生角色');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和部门关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES (2, 100);
INSERT INTO `sys_role_dept` VALUES (2, 101);
INSERT INTO `sys_role_dept` VALUES (2, 105);
INSERT INTO `sys_role_dept` VALUES (3, 100);
INSERT INTO `sys_role_dept` VALUES (3, 101);
INSERT INTO `sys_role_dept` VALUES (3, 103);
INSERT INTO `sys_role_dept` VALUES (3, 104);
INSERT INTO `sys_role_dept` VALUES (3, 105);
INSERT INTO `sys_role_dept` VALUES (3, 106);
INSERT INTO `sys_role_dept` VALUES (3, 107);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (2, 1);
INSERT INTO `sys_role_menu` VALUES (2, 2);
INSERT INTO `sys_role_menu` VALUES (2, 3);
INSERT INTO `sys_role_menu` VALUES (2, 4);
INSERT INTO `sys_role_menu` VALUES (2, 100);
INSERT INTO `sys_role_menu` VALUES (2, 101);
INSERT INTO `sys_role_menu` VALUES (2, 102);
INSERT INTO `sys_role_menu` VALUES (2, 103);
INSERT INTO `sys_role_menu` VALUES (2, 104);
INSERT INTO `sys_role_menu` VALUES (2, 105);
INSERT INTO `sys_role_menu` VALUES (2, 106);
INSERT INTO `sys_role_menu` VALUES (2, 107);
INSERT INTO `sys_role_menu` VALUES (2, 108);
INSERT INTO `sys_role_menu` VALUES (2, 109);
INSERT INTO `sys_role_menu` VALUES (2, 110);
INSERT INTO `sys_role_menu` VALUES (2, 111);
INSERT INTO `sys_role_menu` VALUES (2, 112);
INSERT INTO `sys_role_menu` VALUES (2, 113);
INSERT INTO `sys_role_menu` VALUES (2, 114);
INSERT INTO `sys_role_menu` VALUES (2, 115);
INSERT INTO `sys_role_menu` VALUES (2, 116);
INSERT INTO `sys_role_menu` VALUES (2, 500);
INSERT INTO `sys_role_menu` VALUES (2, 501);
INSERT INTO `sys_role_menu` VALUES (2, 1000);
INSERT INTO `sys_role_menu` VALUES (2, 1001);
INSERT INTO `sys_role_menu` VALUES (2, 1002);
INSERT INTO `sys_role_menu` VALUES (2, 1003);
INSERT INTO `sys_role_menu` VALUES (2, 1004);
INSERT INTO `sys_role_menu` VALUES (2, 1005);
INSERT INTO `sys_role_menu` VALUES (2, 1006);
INSERT INTO `sys_role_menu` VALUES (2, 1007);
INSERT INTO `sys_role_menu` VALUES (2, 1008);
INSERT INTO `sys_role_menu` VALUES (2, 1009);
INSERT INTO `sys_role_menu` VALUES (2, 1010);
INSERT INTO `sys_role_menu` VALUES (2, 1011);
INSERT INTO `sys_role_menu` VALUES (2, 1012);
INSERT INTO `sys_role_menu` VALUES (2, 1013);
INSERT INTO `sys_role_menu` VALUES (2, 1014);
INSERT INTO `sys_role_menu` VALUES (2, 1015);
INSERT INTO `sys_role_menu` VALUES (2, 1016);
INSERT INTO `sys_role_menu` VALUES (2, 1017);
INSERT INTO `sys_role_menu` VALUES (2, 1018);
INSERT INTO `sys_role_menu` VALUES (2, 1019);
INSERT INTO `sys_role_menu` VALUES (2, 1020);
INSERT INTO `sys_role_menu` VALUES (2, 1021);
INSERT INTO `sys_role_menu` VALUES (2, 1022);
INSERT INTO `sys_role_menu` VALUES (2, 1023);
INSERT INTO `sys_role_menu` VALUES (2, 1024);
INSERT INTO `sys_role_menu` VALUES (2, 1025);
INSERT INTO `sys_role_menu` VALUES (2, 1026);
INSERT INTO `sys_role_menu` VALUES (2, 1027);
INSERT INTO `sys_role_menu` VALUES (2, 1028);
INSERT INTO `sys_role_menu` VALUES (2, 1029);
INSERT INTO `sys_role_menu` VALUES (2, 1030);
INSERT INTO `sys_role_menu` VALUES (2, 1031);
INSERT INTO `sys_role_menu` VALUES (2, 1032);
INSERT INTO `sys_role_menu` VALUES (2, 1033);
INSERT INTO `sys_role_menu` VALUES (2, 1034);
INSERT INTO `sys_role_menu` VALUES (2, 1035);
INSERT INTO `sys_role_menu` VALUES (2, 1036);
INSERT INTO `sys_role_menu` VALUES (2, 1037);
INSERT INTO `sys_role_menu` VALUES (2, 1038);
INSERT INTO `sys_role_menu` VALUES (2, 1039);
INSERT INTO `sys_role_menu` VALUES (2, 1040);
INSERT INTO `sys_role_menu` VALUES (2, 1041);
INSERT INTO `sys_role_menu` VALUES (2, 1042);
INSERT INTO `sys_role_menu` VALUES (2, 1043);
INSERT INTO `sys_role_menu` VALUES (2, 1044);
INSERT INTO `sys_role_menu` VALUES (2, 1045);
INSERT INTO `sys_role_menu` VALUES (2, 1046);
INSERT INTO `sys_role_menu` VALUES (2, 1047);
INSERT INTO `sys_role_menu` VALUES (2, 1048);
INSERT INTO `sys_role_menu` VALUES (2, 1049);
INSERT INTO `sys_role_menu` VALUES (2, 1050);
INSERT INTO `sys_role_menu` VALUES (2, 1051);
INSERT INTO `sys_role_menu` VALUES (2, 1052);
INSERT INTO `sys_role_menu` VALUES (2, 1053);
INSERT INTO `sys_role_menu` VALUES (2, 1054);
INSERT INTO `sys_role_menu` VALUES (2, 1055);
INSERT INTO `sys_role_menu` VALUES (2, 1056);
INSERT INTO `sys_role_menu` VALUES (2, 1057);
INSERT INTO `sys_role_menu` VALUES (2, 1058);
INSERT INTO `sys_role_menu` VALUES (2, 1059);
INSERT INTO `sys_role_menu` VALUES (2, 1060);
INSERT INTO `sys_role_menu` VALUES (2, 1061);
INSERT INTO `sys_role_menu` VALUES (3, 1);
INSERT INTO `sys_role_menu` VALUES (3, 103);
INSERT INTO `sys_role_menu` VALUES (3, 1016);
INSERT INTO `sys_role_menu` VALUES (3, 2000);
INSERT INTO `sys_role_menu` VALUES (3, 2001);
INSERT INTO `sys_role_menu` VALUES (3, 2002);
INSERT INTO `sys_role_menu` VALUES (3, 2003);
INSERT INTO `sys_role_menu` VALUES (3, 2004);
INSERT INTO `sys_role_menu` VALUES (3, 2005);
INSERT INTO `sys_role_menu` VALUES (3, 2006);
INSERT INTO `sys_role_menu` VALUES (3, 2007);
INSERT INTO `sys_role_menu` VALUES (3, 2008);
INSERT INTO `sys_role_menu` VALUES (3, 2010);
INSERT INTO `sys_role_menu` VALUES (3, 2013);
INSERT INTO `sys_role_menu` VALUES (3, 2014);
INSERT INTO `sys_role_menu` VALUES (3, 2015);
INSERT INTO `sys_role_menu` VALUES (3, 2017);
INSERT INTO `sys_role_menu` VALUES (4, 2000);
INSERT INTO `sys_role_menu` VALUES (4, 2001);
INSERT INTO `sys_role_menu` VALUES (4, 2002);
INSERT INTO `sys_role_menu` VALUES (4, 2007);
INSERT INTO `sys_role_menu` VALUES (4, 2008);
INSERT INTO `sys_role_menu` VALUES (4, 2009);
INSERT INTO `sys_role_menu` VALUES (4, 2010);
INSERT INTO `sys_role_menu` VALUES (4, 2013);
INSERT INTO `sys_role_menu` VALUES (4, 2014);
INSERT INTO `sys_role_menu` VALUES (4, 2016);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `login_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录账号',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '用户昵称',
  `user_type` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '00' COMMENT '用户类型（00系统用户 01注册用户）',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '手机号码',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '头像路径',
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '密码',
  `salt` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '盐加密',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime(0) DEFAULT NULL COMMENT '最后登录时间',
  `pwd_update_date` datetime(0) DEFAULT NULL COMMENT '密码最后更新时间',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 109 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 100, 'admin', '超级管理员', '00', 'ry@163.com', '15888888888', '1', '', '29c67a30398638269fe600f73a054934', '111111', '0', '0', '127.0.0.1', '2021-06-08 14:12:20', '2021-06-03 18:35:05', 'admin', '2021-06-03 18:35:05', '', '2021-06-08 14:12:20', '管理员');
INSERT INTO `sys_user` VALUES (2, 100, 'ry', '普通管理员', '00', 'ry@qq.com', '15666666666', '1', '', '8e6d98b90472783cc73c17047ddccf36', '222222', '0', '0', '127.0.0.1', '2021-06-03 18:35:05', '2021-06-03 18:35:05', 'admin', '2021-06-03 18:35:05', '', NULL, '测试员');
INSERT INTO `sys_user` VALUES (104, 101, 'zh', '张翰', '00', 'zh@abu.com', '13099999988', '0', '', 'df7f131a25aad819cd1574d7bbd59437', 'de0e95', '0', '0', '127.0.0.1', '2021-06-08 13:34:34', NULL, 'admin', '2021-06-08 12:26:20', '', '2021-06-08 13:34:33', '张翰老师');
INSERT INTO `sys_user` VALUES (105, 101, 'abu', '阿卜', '00', 'abu@abu.com', '13099999999', '0', '', 'fdf2e86b5fa01812887c07a77b48035c', '97aa2a', '0', '0', '127.0.0.1', '2021-06-08 16:06:04', NULL, 'admin', '2021-06-08 13:31:28', '', '2021-06-08 16:06:03', '阿卜老师');
INSERT INTO `sys_user` VALUES (106, 103, 'zs', '张三', '00', 'zs@abu.com', '13099999991', '0', '', '113bf1718912ffe817a03c3775b32650', '0cc291', '0', '0', '127.0.0.1', '2021-06-08 16:00:22', NULL, 'admin', '2021-06-08 13:31:55', '', '2021-06-08 16:00:22', NULL);
INSERT INTO `sys_user` VALUES (107, 104, 'ls', '李四', '00', 'ls@abu.com', '13099999992', '0', '', '7f672ec2882735a429641b818d6fe5cb', 'd79f93', '0', '0', '127.0.0.1', '2021-06-08 13:37:45', NULL, 'admin', '2021-06-08 13:32:16', '', '2021-06-08 13:37:44', NULL);
INSERT INTO `sys_user` VALUES (108, 105, 'wm', '王明', '00', 'wm@abu.com', '13099999993', '0', '', '8901e4713cb17501ab8593dddd8656e4', 'aa6e0e', '0', '0', '', NULL, NULL, 'admin', '2021-06-08 13:32:40', '', NULL, NULL);

-- ----------------------------
-- Table structure for sys_user_online
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_online`;
CREATE TABLE `sys_user_online`  (
  `sessionId` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户会话id',
  `login_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '登录账号',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '部门名称',
  `ipaddr` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '操作系统',
  `status` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '在线状态on_line在线off_line离线',
  `start_timestamp` datetime(0) DEFAULT NULL COMMENT 'session创建时间',
  `last_access_time` datetime(0) DEFAULT NULL COMMENT 'session最后访问时间',
  `expire_time` int(5) DEFAULT 0 COMMENT '超时时间，单位为分钟',
  PRIMARY KEY (`sessionId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '在线用户记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_online
-- ----------------------------
INSERT INTO `sys_user_online` VALUES ('1f5f2b58-e73d-49a8-85cc-c9364f457e01', 'abu', '信息学院', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', 'on_line', '2021-06-08 16:05:49', '2021-06-08 16:12:15', 1800000);
INSERT INTO `sys_user_online` VALUES ('e5fc7b75-7d3d-4a01-96de-5c372952dbc9', 'zs', 'A班', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 10', 'on_line', '2021-06-08 14:17:25', '2021-06-08 16:12:18', 1800000);

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `post_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户与岗位关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES (1, 1);
INSERT INTO `sys_user_post` VALUES (100, 1);
INSERT INTO `sys_user_post` VALUES (101, 2);
INSERT INTO `sys_user_post` VALUES (102, 2);
INSERT INTO `sys_user_post` VALUES (103, 2);
INSERT INTO `sys_user_post` VALUES (104, 1);
INSERT INTO `sys_user_post` VALUES (105, 1);
INSERT INTO `sys_user_post` VALUES (106, 2);
INSERT INTO `sys_user_post` VALUES (107, 2);
INSERT INTO `sys_user_post` VALUES (108, 2);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (100, 3);
INSERT INTO `sys_user_role` VALUES (101, 4);
INSERT INTO `sys_user_role` VALUES (102, 4);
INSERT INTO `sys_user_role` VALUES (103, 4);
INSERT INTO `sys_user_role` VALUES (104, 3);
INSERT INTO `sys_user_role` VALUES (105, 3);
INSERT INTO `sys_user_role` VALUES (106, 4);
INSERT INTO `sys_user_role` VALUES (107, 4);
INSERT INTO `sys_user_role` VALUES (108, 4);

SET FOREIGN_KEY_CHECKS = 1;
